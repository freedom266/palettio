import ProjectDescription
import ProjectDescriptionHelpers

let features: [Feature] = [develop, gallery]
let services: [Service] = []
let appTargets: [Target] = features.flatMap(\.allTargets) + services.flatMap(\.allTargets)

let project = Project(
    name: "Palettio",
    organizationName: "Freedom, s.r.o.",
    options: .options(
        developmentRegion: "en"
    ),
    settings: .settings(
        base: [
            "CODE_SIGN_STYLE": .string(AppSetup.current.codeSignStyle),
            "DEBUG_INFORMATION_FORMAT": "dwarf-with-dsym",
            "DEVELOPMENT_TEAM": "\(AppSetup.current.teamID)",
            "IPHONEOS_DEPLOYMENT_TARGET": "16.0",
            "OTHER_LDFLAGS": "-ObjC",
            "MARKETING_VERSION": "1.0.1",
            "SUPPORTS_MACCATALYST": false
        ],
        configurations: AppSetup.current.projectConfigurations
    ),
    targets: [
        app,
        core,
        designSystem,
        designSystemTests,
        resources,
        testing
    ]
    + appTargets
)
