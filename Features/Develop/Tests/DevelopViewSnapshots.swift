//
//  DevelopViewSnapshots.swift
//  Gallery_Tests
//
//  Created by Martin Svoboda on 22.10.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import XCTest
import PalettioTesting
@testable import Develop

class DevelopViewSnapshots: XCTestCase {
	func testPreviews() {
		let view = DevelopView(viewModel: DevelopViewModelMock())
		AssertSnapshot(view)
	}
}
