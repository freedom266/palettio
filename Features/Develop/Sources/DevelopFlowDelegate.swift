//
//  DevelopFlowDelegate.swift
//  Palettio
//
//  Created by Martin Svoboda on 07.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PalettioCore

public protocol DevelopFlowDelegate: AnyObject {
    func goBack()
}

public func createDevelopViewController<ViewModel: DevelopViewModeling>(
    viewModel: ViewModel
) -> UIViewController {
    let view = DevelopView(viewModel: viewModel)
    return HostingController(rootView: view)
}

public func createDevelopViewModel(
	item: PhotoItem,
	flowDelegate: DevelopFlowDelegate
) -> any DevelopViewModeling {
	let vm = DevelopViewModel(dependencies: dependencies.develop, photoItem: item)
	vm.flowDelegate = flowDelegate
	return vm
}
