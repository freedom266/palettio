//
//  DevelopDependencies.swift
//  Develop
//
//  Created by Martin Svoboda on 20.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import PalettioCore

public struct DevelopDependencies {
	let editToolsProvider: EditToolsProvidering
	let previewSizeProvider: PreviewSizeProvidering
}

extension AppDependency {
	var develop: DevelopDependencies {
		.init(
			editToolsProvider: editToolsProvider, 
			previewSizeProvider: previewSizeProvider
		)
	}
}
