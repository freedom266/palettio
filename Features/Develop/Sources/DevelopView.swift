//
//  DevelopView.swift
//  Palettio
//
//  Created by Martin Svoboda on 07.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PalettioUI
import PalettioResources

struct DevelopView<ViewModel: DevelopViewModeling>: View {
	@ObservedObject var viewModel: ViewModel
	
	var body: some View {
		PTNavigationView(title: L10n.Develop.title, back: viewModel.goBack) {
			GeometryReader { proxy in
				VStack(spacing: 0) {
					Spacer()
					
					if viewModel.isPreviewSizeMeasured {
						PreviewView(viewModel: viewModel)
					} else {
						GeometryView(viewModel: viewModel)
					}
					
					Spacer()
					
					PTPalette(gradientOverlay: $viewModel.gradientOverlay)
				}
				.frame(minHeight: proxy.size.height)
				.frame(maxWidth: .infinity)
			}
		} trailing: {
			trailingView
				.navigationIcon()
				.foregroundColor(.accentPrimary)
		}
		.overlay(alignment: .top) {
			if viewModel.isNotificationShowing {
				PTNotification(content: $viewModel.notificationContent)
			}
		}
		.ignoresSafeArea(.keyboard)
	}
	
	@ViewBuilder private var trailingView: some View {
		if viewModel.keyboardFocus {
			Button(action: viewModel.endWriting) {
				Image(systemName: "checkmark")
			}
		} else {
			Button(action: viewModel.startExport) {
				Image(systemName: "square.and.arrow.up")
			}
		}
	}
}

#Preview {
	DevelopView(viewModel: DevelopViewModelMock())
}
