//
//  CaptionsView.swift
//  Palettio
//
//  Created by Martin Svoboda on 12.10.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PalettioUI

struct CaptionsView<ViewModel: DevelopViewModeling>: View {
	@ObservedObject var viewModel: ViewModel
	
	var body: some View {
		ForEach(Array(viewModel.captionsData.enumerated()), id: \.element.id) { index, data in
			PTCaption(data: data)
				.padding()
				.background(Color.black.opacity(0.001))
				.scaleEffect(data.relativeScale)
				.rotationEffect(.degrees(data.currentRotation))
				.offset(data.currentLocation)
				.gesture(dragGesture(index: index))
				.gesture(rotateGesture(index: index))
		}
	}
	
	private func dragGesture(index: Int) -> some Gesture {
		DragGesture(minimumDistance: 0)
			.onChanged {
				viewModel.updateCurrentLocation(index: index, offset: $0.translation)
			}
			.onEnded { _ in
				viewModel.clearActiveCaption(index: index, isItNewCaptionImage: false)
			}
			.simultaneously(with: tapGesture(index: index))
	}
	
	private func tapGesture(index: Int) -> some Gesture {
		TapGesture()
			.onEnded {
				viewModel.editCaption(index: index)
			}
	}
	
	private func rotateGesture(index: Int) -> some Gesture {
		RotationGesture(minimumAngleDelta: .zero)
			.onChanged {
				viewModel.updateCurrentRotation(index: index, degrees: $0.degrees)
			}
			.onEnded { _ in
				viewModel.clearActiveCaption(index: index, isItNewCaptionImage: true)
			}
			.simultaneously(with: scaleGesture(index: index))
	}
	
	private func scaleGesture(index: Int) -> some Gesture {
		MagnificationGesture(minimumScaleDelta: 0)
			.onChanged {
				viewModel.updateRelativeScale(index: index, scale: $0)
			}
			.onEnded { _ in
				viewModel.clearActiveCaption(index: index, isItNewCaptionImage: true)
			}
	}
}

#Preview {
	CaptionsView(viewModel: DevelopViewModelMock())
		.padding(40)
		.backgroundColor(.backgroundsPrimary)
}
