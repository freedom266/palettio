//
//  GeometryView.swift
//  Palettio
//
//  Created by Martin Svoboda on 20.10.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PalettioUI

struct GeometryView<ViewModel: DevelopViewModeling>: View {
	@ObservedObject var viewModel: ViewModel
	
	var body: some View {
		Image(uiImage: viewModel.photoItem.image)
			.resizable()
			.scaledToFit()
			.overlay {
				GeometryReader { proxy in
					Rectangle()
						.fill(Color.clear)
						.onAppear {
							viewModel.setPreviewSize(
								size: proxy.frame(in: .global).size
							)
						}
				}
			}
	}
}

#Preview {
	GeometryView(viewModel: DevelopViewModelMock())
		.padding()
		.backgroundColor(.backgroundsPrimary)
}
