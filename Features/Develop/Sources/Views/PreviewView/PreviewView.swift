//
//  PreviewView.swift
//  Palettio
//
//  Created by Martin Svoboda on 12.10.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PalettioUI
import PalettioResources

struct PreviewView<ViewModel: DevelopViewModeling>: View {
	@ObservedObject var viewModel: ViewModel
	@FocusState var keyboardFocus: Bool
	
	@State var measuredSize: CGSize = .zero
	
	private var backText: String {
		viewModel.newCaptionData.text.isEmpty ? Texts.captionPlaceholder : viewModel.newCaptionData.text
	}

	typealias Texts = L10n.Develop

	var body: some View {
		Image(uiImage: viewModel.photoItem.image)
			.resizable()
			.frame(
				width: viewModel.previewSize.width,
				height: viewModel.previewSize.height
			)
			.overlay {
				viewModel.gradientOverlay.view
				
				rulersView
				
				CaptionsView(viewModel: viewModel)

				if viewModel.isTrashShowing {
					trashView
				}
				
				if viewModel.keyboardFocus {
					newCaptionTextField
				}
			}
			.overlay(alignment: .top) {
				if viewModel.keyboardFocus {
					PTColorPicker(selectedColor: $viewModel.newCaptionData.color.primary) {
						viewModel.newCaptionData.color.primary = $0
					}
				}
			}
			.onAnyChange(of: viewModel.thumbnailTrigger) { _ in
				viewModel.makeThumbnail(view: self)
			}
			.mask(Color.black)
			.sync($viewModel.keyboardFocus, with: _keyboardFocus)
			.onTapGesture {
				viewModel.startWriting()
			}
	}
	
	private var rulersView: some View {
		ZStack {
			if viewModel.isHorizontalRulerActive {
				rulerView(isVertical: false)
					.frame(width: 1.5)
			}

			if viewModel.isVerticalRulerActive {
				rulerView(isVertical: true)
					.frame(height: 1.5)
			}
		}
		.sensoryFeedback(trigger: viewModel.isVerticalRulerActive)
		.sensoryFeedback(trigger: viewModel.isHorizontalRulerActive)
		.sensoryFeedback(trigger: viewModel.isRotationRulerActive)
		.sensoryFeedback(trigger: viewModel.isTrashOpen)
	}
	
	private var trashView: some View {
		Image(systemName: "trash.circle")
			.font(.system(size: viewModel.isTrashOpen ? 60 : 45, weight: .ultraLight))
			.foregroundColor(.deletePrimary)
			.offset(viewModel.trashLocation)
	}
	
	private var newCaptionTextField: some View {
		ZStack {
			Color.black
				.opacity(0.5)
				.ignoresSafeArea()
				.onTapGesture {
					viewModel.endWriting()
				}
			
			ZStack {
				Text(backText)
					.measureSizeOnChange(of: viewModel.newCaptionData.text) { size in
						measuredSize = size
					}
					.opacity(0)
				
				TextField(Texts.captionPlaceholder, text: $viewModel.newCaptionData.text, axis: .vertical)
					.focused($keyboardFocus)
					.frame(width: measuredSize.width, height: measuredSize.height)
			}
			.captionButton(primaryColor: viewModel.newCaptionData.color.primary)
		}
	}
	
	private func rulerView(isVertical: Bool) -> some View {
		Rectangle()
			.fill(
				LinearGradient(
					colors: [.clear, .white, .clear],
					startPoint: isVertical ? .leading : .top,
					endPoint: isVertical ? .trailing : .bottom
				)
			)
			.opacity(0.75)
	}
}

#Preview {
	PreviewView(viewModel: DevelopViewModelMock())
		.padding()
		.backgroundColor(.backgroundsPrimary)
}
