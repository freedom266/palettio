//
//  DevelopViewModel.swift
//  Palettio
//
//  Created by Martin Svoboda on 07.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import Combine
import PalettioCore

public protocol DevelopViewModeling: ObservableObject {
	var photoItem: PhotoItem { get }
	var thumbnailTrigger: Bool { get set }

	/// Overlay
	var gradientOverlay: GradientOverlay { get set }
	
	/// Captions
	var newCaptionData: CaptionData { get set }
	var captionsData: [CaptionData] { get }
	var keyboardFocus: Bool { get set }
	
	/// Notification
	var isNotificationShowing: Bool { get }
	var notificationContent: NotificationContent { get set }
	
	/// Rulers
	var isVerticalRulerActive: Bool { get }
	var isHorizontalRulerActive: Bool { get }
	var isRotationRulerActive: Bool { get }
	
	/// Trash
	var isTrashShowing: Bool { get }
	var isTrashOpen: Bool { get }
	var trashLocation: CGSize { get }
	
	/// Preview
	var isPreviewSizeMeasured: Bool { get }
	var previewSize: CGSize { get }
	
	func setPreviewSize(size: CGSize)
	
	func startWriting()
	func endWriting()
	func startExport()
	func goBack()
	
	func editCaption(index: Int)
	
	func updateCurrentLocation(index: Int, offset: CGSize)
	func updateCurrentRotation(index: Int, degrees: CGFloat)
	func updateRelativeScale(index: Int, scale: CGFloat)
	func clearActiveCaption(index: Int, isItNewCaptionImage: Bool)
	
	func makeThumbnail(view: some View)
}

final class DevelopViewModel: BaseViewModel, DevelopViewModeling {
	@Published var thumbnailTrigger = false

	@Published var gradientOverlay: GradientOverlay
	@Published var captionsData: [CaptionData]
	
	@Published var newCaptionData = CaptionData()
	@Published var keyboardFocus = false

	@Published var isNotificationShowing = false
	@Published var notificationContent = NotificationContent()
	
	@Published var isVerticalRulerActive = false
	@Published var isHorizontalRulerActive = false
	@Published var isRotationRulerActive = false

	@Published var previewSize: CGSize = .zero
	@Published var isPreviewSizeMeasured = false

	@Published var isTrashShowing = false
	@Published var isTrashOpen = false
	var trashLocation: CGSize { previewSizeProvider.trashLocation }
	
	var originImage: UIImage
	var mergingImage: UIImage
	var captionImages: [String: UIImage] = [:]

	let photoItem: PhotoItem
	
    weak var flowDelegate: DevelopFlowDelegate?
	private let previewSizeProvider: PreviewSizeProvidering
	private let editToolsProvider: EditToolsProvidering
	private var cancellables = Set<AnyCancellable>()

    // MARK: Init

    init(dependencies: DevelopDependencies, photoItem: PhotoItem) {
		self.editToolsProvider = dependencies.editToolsProvider
		self.previewSizeProvider = dependencies.previewSizeProvider
		self.photoItem = photoItem
		
		self.originImage = photoItem.image
		self.mergingImage = photoItem.image
		
		self.gradientOverlay = photoItem.overlay
		self.captionsData = photoItem.captions
		
		super.init()
		setupBindings()
    }

    // MARK: Public API
	
    func goBack() {
        flowDelegate?.goBack()
    }
	
	func setPreviewSize(size: CGSize) {
		previewSizeProvider.setPreviewSize(size: size)
	}
	
	func updateCurrentLocation(index: Int, offset: CGSize) {
		captionsData[index].updateCurrentLocation(offset: offset)
		editToolsProvider.updateActiveCaption(data: captionsData[index])
		
		withAnimation(.easeInOut(duration: .trashAnimDuration)) { [weak self] in
			guard let self else { return }
			captionsData[index].updateScaleBasedOnTrash(isTrashOpen)
		}
	}
	
	func updateCurrentRotation(index: Int, degrees: CGFloat) {
		captionsData[index].updateCurrentRotation(degrees: degrees)
		editToolsProvider.updateActiveCaption(data: captionsData[index])
	}
	
	func updateRelativeScale(index: Int, scale: CGFloat) {
		captionsData[index].updateRelativeScale(scale)
	}
	
	func clearActiveCaption(index: Int, isItNewCaptionImage: Bool) {
		if isTrashOpen {
			withAnimation(.linear(duration: .deleteAnimDuration)) {
				deleteCaption(index: index)
			}
		} else {
			captionsData[index].updateLastLocation()
			captionsData[index].updateLastRotation()
			captionsData[index].updateGlobalScale()
		}
		
		editToolsProvider.clearActiveCaption()
		update(captions: captionsData)
		
		guard isItNewCaptionImage, !isTrashOpen else { return }
		makeCaptionImage(data: captionsData[index])
	}
	
	func editCaption(index: Int) {
		let captionHasToMove = !captionsData[index].isLocationOrigin
		let animDuration = captionHasToMove ? .editAnimDuration : 0
		
		if captionHasToMove {
			withAnimation(.linear(duration: .editAnimDuration)) {
				captionsData[index].transform.prepareForMove()
			}
		}
		
		DispatchQueue.main.asyncAfter(deadline: .now() + animDuration * 2) { [weak self] in
			guard let self, index < captionsData.count else { return }
			newCaptionData = captionsData[index]
			deleteCaption(index: index)
			keyboardFocus = true
		}
	}
	
	func startWriting() {
		keyboardFocus = true
	}
	
	func endWriting() {
		keyboardFocus = false
		
		if newCaptionData.text.isNotEmpty {
			addCaption(with: newCaptionData)
		}
		
		newCaptionData.clear()
	}
	
	/// Screenshoting preview for thumbnail
	func makeThumbnail(view: some View) {
		DispatchQueue.main.asyncAfter(deadline: .now() + .screenshotDelay) {
			let thumbnailRender = ImageRenderer(content: view)
			
			if let thumbnail = thumbnailRender.uiImage {
				Task(priority: .utility) { [weak self] in
					guard let self else { return }
					photoItem.update(thumbnail: thumbnail)
				}
			}
		}
	}
	
	// MARK: Private API
	
	private func addCaption(with data: CaptionData) {
		captionsData.append(data)
		update(captions: captionsData)
		
		makeCaptionImage(data: data)

		withAnimation(.linear(duration: .editAnimDuration)) {
			captionsData[captionsData.count - 1].transform.restorePreviousTransform()
		}
	}
	
	private func deleteCaption(index: Int) {
		captionsData.remove(at: index)
		update(captions: captionsData)
	}
	
	private func applyOverlay(overlay: GradientOverlay) {
		DispatchQueue.main.async { [weak self] in
			let imageRender = ImageRenderer(
				content: overlay.view.frame(
					width: self?.photoItem.image.size.width,
					height: self?.photoItem.image.size.height
				)
			)
			if let topImage = imageRender.uiImage {
				DispatchQueue.global(qos: .userInitiated).async { [weak self] in
					guard let self else { return }
					mergingImage = originImage.mergeWith(topImage: topImage)
				}
			}
		}
	}
	
	private func update(captions: [CaptionData]) {
		Task(priority: .utility) { [weak self] in
			guard let self else { return }
			await photoItem.update(captions: captions)
		}
		thumbnailTrigger.toggle()
	}
	
	private func update(overlay: GradientOverlay) {
		Task(priority: .utility) { [weak self] in
			guard let self else { return }
			await photoItem.update(overlay: overlay)
		}
		thumbnailTrigger.toggle()
	}
	
	private func setupBindings() {
		$gradientOverlay
			.debounce(for: 0.5, scheduler: DispatchQueue.main)
			.sink { [weak self] overlay in
				self?.applyOverlay(overlay: overlay)
				self?.update(overlay: overlay)
			}
			.store(in: &cancellables)
		
		previewSizeProvider.previewSize
			.dropFirst()
			.sink { [weak self] size in
				self?.previewSize = size
				self?.isPreviewSizeMeasured = true
				self?.newCaptionData.setPreviewSize(size: size)
			}
			.store(in: &cancellables)
		
		editToolsProvider.isTrashShowing
			.debounce(for: !isTrashShowing ? 0.2 : 0, scheduler: DispatchQueue.main)
			.sink { isShowing in
				withAnimation(.easeInOut(duration: .trashAnimDuration)) { [weak self] in
					self?.isTrashShowing = isShowing
				}
			}
			.store(in: &cancellables)
		
		editToolsProvider.isTrashOpen
			.sink { isOpen in
				withAnimation(.easeInOut(duration: .trashAnimDuration)) { [weak self] in
					self?.isTrashOpen = isOpen
				}
			}
			.store(in: &cancellables)
		
		editToolsProvider.isVerticalRulerActive
			.sink { isActive in
				withAnimation { [weak self] in
					self?.isVerticalRulerActive = isActive
				}
			}
			.store(in: &cancellables)
		
		editToolsProvider.isHorizontalRulerActive
			.sink { isActive in
				withAnimation { [weak self] in
					self?.isHorizontalRulerActive = isActive
				}
			}
			.store(in: &cancellables)
		
		editToolsProvider.isRotationRulerActive
			.weakAssign(to: \.isRotationRulerActive, on: self)
			.store(in: &cancellables)
	}
}
