//
//  DevelopViewModel+Export.swift
//  Develop
//
//  Created by Martin Svoboda on 03.11.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import Photos
import PalettioUI
import PalettioCore
import PalettioResources

extension DevelopViewModel {
	@MainActor func startExport() {
		notificationContent = .init(type: .loading(L10n.Develop.exporting))
		
		DispatchQueue.main.async { [weak self] in
			withAnimation {
				self?.isNotificationShowing = true
			}
		}
		
		exportImage()
	}
	
	func makeCaptionImage(data: CaptionData) {
		let renderedCaption = PTCaption(data: data, scale: exportScale)
		DispatchQueue.main.async { [weak self] in
			if let captionImage = ImageRenderer(content: renderedCaption).uiImage {
				guard let self else { return }
				let rotation = data.currentRotation
				let captionImage = captionImage.rotate(degrees: rotation)
				captionImages[data.id] = captionImage
			}
		}
	}
}
	
private extension DevelopViewModel {
	@MainActor func endExport(notificationType: NotificationType) {
		notificationContent = .init(type: notificationType)
		
		DispatchQueue.main.asyncAfter(deadline: .now() + 3) { [weak self] in
			withAnimation {
				self?.isNotificationShowing = false
			}
		}
	}
	
	@MainActor func exportImage() {
		let originMergingImage = mergingImage
		captionsData.forEach { data in
			if let captionImage = captionImage(data: data) {
				let mergeArea = mergeArea(image: captionImage, location: data.currentLocation)
				mergingImage = mergingImage.mergeWith(
					topImage: captionImage,
					mergeArea: mergeArea
				)
			}
		}
		
		// Request access to the photo library
		PHPhotoLibrary.requestAuthorization { [weak self] status in
			switch status {
			case .notDetermined, .restricted, .denied:
				guard let self else { return }
				mergingImage = originMergingImage
				DispatchQueue.main.async { [weak self] in
					self?.endExport(notificationType: .fail(L10n.Develop.failed))
				}
			case .authorized, .limited:
				guard let self else { return }
				UIImageWriteToSavedPhotosAlbum(mergingImage, nil, nil, nil)
				mergingImage = originMergingImage
				DispatchQueue.main.async { [weak self] in
					self?.endExport(notificationType: .success(L10n.Develop.saved))
				}
			@unknown default:
				break
			}
		}
	}
	
	@MainActor func captionImage(data: CaptionData) -> UIImage? {
		if let captionImage = captionImages[data.id] {
			return captionImage
		}
		
		let captionView = PTCaption(data: data, scale: exportScale)
		let captionImage = ImageRenderer(content: captionView).uiImage
		return captionImage?.rotate(degrees: data.currentRotation)
	}
	
	func mergeArea(image: UIImage, location: CGSize) -> CGRect {
		let x = (previewSize.width / 2 + location.width) * exportScale
		let y = (previewSize.height / 2 + location.height) * exportScale
		return CGRect(
			x: x - image.size.width / 2,
			y: y - image.size.height / 2,
			width: image.size.width,
			height: image.size.height
		)
	}
}

private extension DevelopViewModel {
	var exportScale: CGFloat {
		originImage.size.width / previewSize.width
	}
}
