//
//  DevelopViewModelMock.swift
//  Develop
//
//  Created by Martin Svoboda on 10.10.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PalettioUI
import PalettioCore
import PalettioResources

final class DevelopViewModelMock: DevelopViewModeling {
	var photoItem: PhotoItem = .mock9()
	var thumbnailTrigger = false
	
	/// Overlay
	@Published var gradientOverlay: GradientOverlay = .init(
		colorA: .red,
		colorB: .blue,
		opacity: 0.5,
		angle: 0.1
	)
	
	/// Captions
	@Published var newCaptionData: CaptionData = .init()
	@Published var captionsData: [CaptionData] = []
	@Published var keyboardFocus: Bool = false
	
	/// Notification
	@Published var isNotificationShowing = false
	@Published var notificationContent: NotificationContent = .init()
	
	/// Rulers
	var isHorizontalRulerActive: Bool = false
	var isVerticalRulerActive: Bool = false
	var isRotationRulerActive: Bool = false
	
	/// Trash
	var isTrashShowing: Bool = false
	var isTrashOpen: Bool = false
	var trashLocation: CGSize { .init(width: 0, height: previewSize.height / 2 - 50) }
	
	/// Preview
	var previewSize: CGSize = .init(width: 333, height: 444)
	var isPreviewSizeMeasured: Bool { previewSize != .zero }
	
	// MARK: Public API
	
	func setPreviewSize(size: CGSize) { previewSize = size }
	
	func startWriting() { }
	func endWriting() { }
	func goBack() { }

	func editCaption(index: Int) { }
	
	func updateCurrentLocation(index: Int, offset: CGSize) { }
	func updateCurrentRotation(index: Int, degrees: CGFloat) { }
	func updateRelativeScale(index: Int, scale: CGFloat) { }
	func clearActiveCaption(index: Int, isItNewCaptionImage: Bool) { }
	
	func makeThumbnail(view: some View) { }
	
	func startExport() {
		notificationContent = .init(type: .loading(L10n.Develop.exporting))
		
		DispatchQueue.main.async { [weak self] in
			withAnimation {
				self?.isNotificationShowing = true
			}
		}
		
		DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
			self?.notificationContent = .init(type: .success(L10n.Develop.saved))
		}
		
		DispatchQueue.main.asyncAfter(deadline: .now() + 3) { [weak self] in
			withAnimation {
				self?.isNotificationShowing = false
			}
		}
	}
}
