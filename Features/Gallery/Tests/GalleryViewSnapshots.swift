//
//  GalleryViewSnapshots.swift
//  Gallery_Tests
//
//  Created by Martin Svoboda on 22.10.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import XCTest
import PalettioTesting
@testable import Gallery

class GalleryViewSnapshots: XCTestCase {
	var viewModel: GalleryViewModelMock!

	override func setUp() {
		super.setUp()
		viewModel = GalleryViewModelMock()
	}

	override func tearDown() {
		viewModel = nil
		super.tearDown()
	}
	
	func testEmptyGallery() {
		viewModel.thumbnailVMs = []
		let view = GalleryView(viewModel: viewModel)
		AssertSnapshot(view)
	}
	
	func testWithPhotos() {
		let view = GalleryView(viewModel: viewModel)
		AssertSnapshot(view)
	}
	
	func testDeleteAlert() {
		viewModel.isSelectionActive = true
		viewModel.isAlertShown = true
		let view = GalleryView(viewModel: viewModel)
		AssertSnapshot(view)
	}
}
