//
//  GalleryViewModelMock.swift
//  Develop
//
//  Created by Martin Svoboda on 10.10.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PhotosUI
import PalettioCore

final class GalleryViewModelMock: GalleryViewModeling {
	@Published var thumbnailVMs: [ThumbnailViewModel] = [PhotoItem].mock()
		.map { item in
			ThumbnailViewModel(
				item: item,
				isSelectionActive: { false },
				goToDevelop: { _ in }
			)
		}
	
	@Published var isSelectionActive: Bool = false
	@Published var isAlertShown = false
	@Published var newPhoto: PhotosPickerItem?
	
	func showAlert() { }
	func deletePhotos() { }
	func resetSelection() { }
}
