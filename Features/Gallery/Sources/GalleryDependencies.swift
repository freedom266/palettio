//
//  GalleryDependencies.swift
//  Palettio
//
//  Created by Martin Svoboda on 07.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import PalettioCore

struct GalleryDependencies {
	let photoItemsRepository: PhotoItemsRepositoring
}

extension AppDependency {
	var gallery: GalleryDependencies {
		.init(
			photoItemsRepository: photoItemsRepository
		)
	}
}
