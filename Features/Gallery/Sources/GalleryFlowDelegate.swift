//
//  GalleryFlowDelegate.swift
//  Palettio
//
//  Created by Martin Svoboda on 07.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PalettioCore

public protocol GalleryFlowDelegate: AnyObject {
    func goToDevelop(item: PhotoItem)
    func goBack()
}

public func createGalleryViewController<ViewModel: GalleryViewModeling>(
    viewModel: ViewModel
) -> UIViewController {
	let view = GalleryView(viewModel: viewModel)
    return HostingController(rootView: view)
}

public func createGalleryViewModel(
	flowDelegate: GalleryFlowDelegate
) -> any GalleryViewModeling {
	let vm = GalleryViewModel(dependencies: dependencies.gallery)
	vm.flowDelegate = flowDelegate
	return vm
}
