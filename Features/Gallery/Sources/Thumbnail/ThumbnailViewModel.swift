//
//  ThumbnailViewModel.swift
//  Gallery
//
//  Created by Martin Svoboda on 20.10.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import Combine
import PalettioCore

public protocol ThumbnailViewModeling: ObservableObject {
	var item: PhotoItem { get }
	var thumbnail: UIImage? { get }
	var isSelected: Bool { get set }
	var isPressed: Bool { get set }
}

public final class ThumbnailViewModel: ThumbnailViewModeling {
	@Published public var thumbnail: UIImage?
	@Published public var isSelected = false
	@Published public var isPressed = false
	@Published public var trigger = false
	
	public var item: PhotoItem
	
	private let isSelectionActive: () -> Bool
	private let goToDevelop: (PhotoItem) -> Void
	private let action: (() -> Void)?
	
	private var isActionEnabled = false
	private let longPressDelay: Double = 0.25
	private var cancellables = Set<AnyCancellable>()
	
	// MARK: Init

	public init(
		item: PhotoItem,
		isSelectionActive: @escaping () -> Bool,
		goToDevelop: @escaping (PhotoItem) -> Void,
		action: (() -> Void)? = nil
	) {
		self.item = item
		self.thumbnail = item.thumbnail
		self.isSelectionActive = isSelectionActive
		self.goToDevelop = goToDevelop
		self.action = action
		setupBindings()
	}

	// MARK: Private API

	private func setupBindings() {
		item.thumbnailPublisher
			.receive(on: DispatchQueue.main)
			.weakAssign(to: \.thumbnail, on: self)
			.store(in: &cancellables)
		
		$isPressed
			.sink { [weak self] isPressed in
				guard let self else { return }
				
				if isPressed {
					isActionEnabled = true
					
					DispatchQueue.main.asyncAfter(deadline: .now() + longPressDelay) { [weak self] in
						guard let self else { return }
						
						if isActionEnabled {
							isSelected = true
							action?()
							isActionEnabled = false
						}
						
					}
				} else if isActionEnabled {
					if isSelectionActive() {
						DispatchQueue.main.async { [weak self] in
							self?.isSelected.toggle()
							self?.action?()
						}
					} else {
						goToDevelop(item)
					}
					isActionEnabled = false
				}
			}
			.store(in: &cancellables)
	}
}

extension ThumbnailViewModel: Identifiable {
	public var id: ObjectIdentifier { item.id }
}
