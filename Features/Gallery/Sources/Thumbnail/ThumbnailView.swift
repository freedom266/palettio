//
//  ThumbnailView.swift
//  Palettio
//
//  Created by Martin Svoboda on 19.10.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PalettioCore
import PalettioUI

struct ThumbnailView<ViewModel: ThumbnailViewModeling>: View {
	@ObservedObject var viewModel: ViewModel
	
    var body: some View {
		Button {
			
		} label: {
			Rectangle()
				.fill(.backgroundsPrimary)
				.aspectRatio(1, contentMode: .fit)
				.overlay {
					Image(uiImage: viewModel.thumbnail ?? .init())
						.resizable()
						.scaledToFill()
				}
				.clipped()
				.overlay {
					selectionOverlay
						.opacity(viewModel.isSelected ? 1 : 0)
				}
		}
		.buttonStyle(MultiButtonStyle(isPressed: $viewModel.isPressed))
    }
	
	private var selectionOverlay: some View {
		Rectangle()
			.fill(
				LinearGradient(
					colors: [.black, .clear],
					startPoint: .top,
					endPoint: .bottom
				)
			)
			.opacity(0.75)
			.overlay(alignment: .bottomTrailing) {
				Image(systemName: "checkmark.circle.fill")
					.font(.title2)
					.foregroundStyle(.deletePrimary, .accentPrimary)
					.padding(8)
			}
	}
}

extension ThumbnailView {
	struct MultiButtonStyle: ButtonStyle {
		@Binding var isPressed: Bool
		
		func makeBody(configuration: Configuration) -> some View {
			if #available(iOS 17.0, *) {
				return configuration.label
					.onChange(of: configuration.isPressed) {
						isPressed = configuration.isPressed
					}
			} else {
				return configuration.label
					.onChange(of: configuration.isPressed) { newValue in
						isPressed = newValue
					}
			}
		}
	}
}

#Preview {
	VStack {
		ThumbnailView(viewModel: ThumbnailViewModel(item: .mock1()))
		ThumbnailView(viewModel: ThumbnailViewModel(item: .mock2()))
		ThumbnailView(viewModel: ThumbnailViewModel(item: .mock3()))
	}
	.padding(50)
	
}

fileprivate extension ThumbnailViewModel {
	convenience init(item: PhotoItem) {
		self.init(
			item: item,
			isSelectionActive: { false },
			goToDevelop: { _ in }
		)
	}
}
