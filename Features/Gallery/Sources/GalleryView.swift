//
//  GalleryView.swift
//  Palettio
//
//  Created by Martin Svoboda on 07.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PhotosUI
import PalettioUI
import PalettioResources

struct GalleryView<ViewModel: GalleryViewModeling>: View {
	@ObservedObject var viewModel: ViewModel
	
	typealias Texts = L10n.Gallery

	var gridColumns: [GridItem] {
		let isPhone = UIDevice.current.userInterfaceIdiom == .phone
		let columnsCount = isPhone ? 2 : 3
		
		return .init(repeating: GridItem(.flexible(), spacing: 3), count: columnsCount)
	}
	
	var body: some View {
		PTNavigationView(title: Texts.title) {
			if viewModel.thumbnailVMs.isNotEmpty {
				ScrollView {
					LazyVGrid(columns: gridColumns, spacing: 3) {
						ForEach(viewModel.thumbnailVMs) { vm in
							ThumbnailView(viewModel: vm)
						}
					}
				}
			} else {
				galleryPlaceholder
			}
		} trailing: {
			if viewModel.isSelectionActive {
				selectionBar
			} else {
				galleryBar
			}
		}
		.sensoryFeedback(trigger: viewModel.isSelectionActive)
		.destructiveAlert(
			isPresented: $viewModel.isAlertShown,
			title: Texts.alertTitle,
			message: Texts.alertMessage,
			primaryAction: viewModel.deletePhotos
		)
	}
	
	private var galleryBar: some View {
		PhotosPicker(
			selection: $viewModel.newPhoto,
			matching: .images,
			photoLibrary: .shared()
		) {
			Image(systemName: "plus")
				.navigationIcon()
				.foregroundColor(.accentPrimary)
		}
	}
	
	private var selectionBar: some View {
		HStack {
			Button(action: viewModel.showAlert) {
				Image(systemName: "trash")
					.navigationIcon()
					.foregroundColor(.accentPrimary)
					.padding(.leading, 20)
			}
			
			Spacer()
			
			Button(action: viewModel.resetSelection) {
				Text(Texts.done)
					.navigationText()
					.foregroundColor(.accentPrimary)
					.padding(.leading, 20)
			}
		}
	}
	
	private var galleryPlaceholder: some View {
		VStack {
			Spacer()
			
			Text(Texts.placeholder)
				.paragraphNormal()
				.foregroundColor(.foregroundsSecondary)
				.padding(.horizontal, 50)
				.multilineTextAlignment(.center)
			
			Spacer()
			Spacer()
		}
	}
}

#Preview {
	GalleryView(viewModel: GalleryViewModelMock())
}
