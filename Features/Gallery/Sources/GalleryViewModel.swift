//
//  GalleryViewModel.swift
//  Palettio
//
//  Created by Martin Svoboda on 07.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import Combine
import PhotosUI
import PalettioCore

public protocol GalleryViewModeling: ObservableObject {
	var thumbnailVMs: [ThumbnailViewModel] { get }
	var isSelectionActive: Bool { get }
	var isAlertShown: Bool { get set }
	var newPhoto: PhotosPickerItem? { get set }
	
	func showAlert()
	func deletePhotos()
	func resetSelection()
}

final class GalleryViewModel: BaseViewModel, GalleryViewModeling {
	@Published var thumbnailVMs: [ThumbnailViewModel] = []
	@Published var newPhoto: PhotosPickerItem?
	@Published var isAlertShown = false
	@Published var isSelectionActive: Bool = false
	
	weak var flowDelegate: GalleryFlowDelegate?
	private let photoItemsRepository: PhotoItemsRepositoring
	private var cancellables = Set<AnyCancellable>()
	
	// MARK: Init
	
	init(dependencies: GalleryDependencies) {
		self.photoItemsRepository = dependencies.photoItemsRepository
		super.init()
		setupBindings()
	}
	
	// MARK: Public API

	func showAlert() {
		isAlertShown = true
	}
	
	func resetSelection() {
		thumbnailVMs.forEach { vm in
			vm.isSelected = false
		}
		isSelectionActive = false
	}
	
	func deletePhotos() {
		thumbnailVMs.forEach { vm in
			if vm.isSelected {
				photoItemsRepository.delete(item: vm.item)
			}
		}
		resetSelection()
	}
	
	// MARK: Private API
	
	private func setupBindings() {
		photoItemsRepository.items
			.receive(on: DispatchQueue.main)
			.map {
				$0.sorted {
					// From newest to oldest
					$0.url.absoluteString > $1.url.absoluteString
				}
				.map { item in
					ThumbnailViewModel(
						item: item,
						isSelectionActive: { self.isSelectionActive },
						goToDevelop: self.goToDevelop,
						action: { self.updateSelection() }
					)
				}
			}
			.sink { newValue in
				withAnimation { [weak self] in
					self?.thumbnailVMs = newValue
				}
			}
			.store(in: &cancellables)
		
		$newPhoto
			.dropFirst()
			.sink { [weak self] newPhoto in
				self?.addPhoto(newPhoto)
			}
			.store(in: &cancellables)
	}
	
	private func goToDevelop(item: PhotoItem) {
		flowDelegate?.goToDevelop(item: item)
	}
	
	private func updateSelection() {
		isSelectionActive = thumbnailVMs.contains { $0.isSelected }
	}
	
	private func addPhoto(_ newPhoto: PhotosPickerItem?) {
		Task { [weak self] in
			await self?.photoItemsRepository.insert(newPhoto: newPhoto)
			DispatchQueue.main.async { [weak self] in
				self?.newPhoto = nil
			}
		}
	}
}
