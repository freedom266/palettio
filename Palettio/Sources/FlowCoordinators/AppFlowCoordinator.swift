//
//  AppFlowCoordinator.swift
//  Palettio
//
//  Created by Martin Svoboda on 07.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PalettioCore

final class AppFlowCoordinator: BaseFlowCoordinator {
    private var window: UIWindow!

    override func start(in window: UIWindow) {
        self.window = window

        super.start(in: window)

        let galleryCoordinator = GalleryFlowCoordinator()
        galleryCoordinator.delegate = self
        startChildCoordinator(galleryCoordinator)
    }

    private func startChildCoordinator(_ coordinator: BaseFlowCoordinator) {
        addChild(coordinator)
        let vc = coordinator.start()

        window.rootViewController = vc
        rootViewController = vc

        activeChild = coordinator
    }
}

extension AppFlowCoordinator: GalleryFlowCoordinatorDelegate {

}
