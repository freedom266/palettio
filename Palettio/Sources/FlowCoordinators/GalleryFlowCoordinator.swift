//
//  GalleryFlowCoordinator.swift
//  Palettio
//
//  Created by Martin Svoboda on 07.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import UIKit
import PalettioCore
import Develop
import Gallery

protocol GalleryFlowCoordinatorDelegate: AnyObject {

}

final class GalleryFlowCoordinator: BaseFlowCoordinator {
    weak var delegate: GalleryFlowCoordinatorDelegate?

    override func start() -> UIViewController {
        super.start()

        let vc = createGalleryViewController(
            viewModel: createGalleryViewModel(
                flowDelegate: self
            )
        )

        let navVC = NavigationController(rootViewController: vc)
        rootViewController = vc
        navigationController = navVC

        return navVC
    }
}

extension GalleryFlowCoordinator: DevelopFlowDelegate {
    func goBack() {
        navigationController?.popViewController(animated: true)
    }
}

extension GalleryFlowCoordinator: GalleryFlowDelegate {
    func goToDevelop(item: PhotoItem) {
        let vc = createDevelopViewController(
            viewModel: createDevelopViewModel(
                item: item,
                flowDelegate: self
            )
        )
        navigationController?.pushViewController(vc, animated: true)
    }
}
