//
//  AppDelegate.swift
//  Palettio
//
//  Created by Martin Svoboda on 07.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import UIKit

@main
final class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    private lazy var appFlowCoordinator = AppFlowCoordinator()

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil
    ) -> Bool {

        let window = UIWindow(frame: UIScreen.main.bounds)
        appFlowCoordinator.start(in: window)
        window.makeKeyAndVisible()
        self.window = window

        return true
    }
}
