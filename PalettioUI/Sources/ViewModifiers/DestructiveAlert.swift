//
//  DestructiveAlert.swift
//  Palettio
//
//  Created by Martin Svoboda on 15.12.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

struct DestructiveAlertModifier: ViewModifier {
	@Binding var isPresented: Bool
	
	let title: String
	let message: String
	let primaryAction: () -> Void
	let secondaryAction: (() -> Void)?
	
	func body(content: Content) -> some View {
		content
			.disabled(isPresented)
			.overlay {
				if isPresented {
					PTDestructiveAlert(
						isPresented: $isPresented,
						title: title,
						message: message,
						primaryAction: primaryAction,
						secondaryAction: secondaryAction
					)
				}
			}
	}
}

public extension View {
	func destructiveAlert(
		isPresented: Binding<Bool>,
		title: String,
		message: String,
		primaryAction: @escaping () -> Void,
		secondaryAction: (() -> Void)? = nil
	) -> some View {
		modifier(
			DestructiveAlertModifier(
				isPresented: isPresented,
				title: title,
				message: message,
				primaryAction: primaryAction,
				secondaryAction: secondaryAction
			)
		)
	}
}
