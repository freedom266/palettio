//
//  View+EndEditingOnTap.swift
//  Palettio
//
//  Created by Martin Svoboda on 06.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

public extension View {
	@available(iOSApplicationExtension, unavailable)
	func endEditingOnTap() -> some View {
		onTapGesture {
			UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
		}
	}
}

public extension View {
	func sync<T: Equatable>(_ binding: Binding<T>, with focusState: FocusState<T>) -> some View {
		self
			.onAnyChange(of: binding.wrappedValue) { binding in
				focusState.wrappedValue = binding
			}
			.onAnyChange(of: focusState.wrappedValue) { focusState in
				binding.wrappedValue = focusState
			}
	}
}

public extension View {
	func sensoryFeedback(trigger: Bool) -> some View {
		if #available(iOS 17.0, *) {
			return sensoryFeedback(trigger: trigger) { oldValue, newValue in
				!oldValue && newValue ? .impact() : nil
			}
		} else {
			return self
		}
	}
}

public extension View {
	func onAnyChange<V>(of value: V, action: @escaping (V) -> Void) -> some View where V : Equatable {
		if #available(iOS 17.0, *) {
			return self
				.onChange(of: value) {
					action(value)
				}
		} else {
			return self
				.onChange(of: value) { newValue in
					action(newValue)
				}
		}
	}
}

public extension View {
	func measureSizeOnChange<V>(
		of value: V,
		action: @escaping (CGSize) -> Void
	) -> some View where V : Equatable {
		overlay {
			GeometryReader { proxy in
				Color.clear
					.onAppear {
						action(proxy.size)
					}
					.onAnyChange(of: value) { _ in
						action(proxy.size)
					}
			}
		}
	}
}
