//
//  InstagramSansHeadline.swift
//  PalettioUI
//
//  Created by Martin Svoboda on 28.12.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PalettioResources

public enum InstagramSansHeadline {
	case regular

	var font: PalettioResourcesFontConvertible {
		switch self {
		case .regular: return PalettioResourcesFontFamily.InstagramSans.headlineRegular
		}
	}

	public func font(size: CGFloat) -> UIFont {
		font.font(size: size)
	}
}
