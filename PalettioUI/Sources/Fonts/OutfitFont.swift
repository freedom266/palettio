//
//  OutfitFont.swift
//  Palettio
//
//  Created by Martin Svoboda on 09.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PalettioResources

public enum Outfit {
    case regular    // weight 400
    case medium     // weight 500
    case semiBold   // weight 600
    case bold       // weight 700

    var font: PalettioResourcesFontConvertible {
        switch self {
        case .regular: return PalettioResourcesFontFamily.Outfit.regular
        case .medium: return PalettioResourcesFontFamily.Outfit.medium
        case .semiBold: return PalettioResourcesFontFamily.Outfit.semiBold
        case .bold: return PalettioResourcesFontFamily.Outfit.bold
        }
    }

    public func font(size: CGFloat) -> UIFont {
        font.font(size: size)
    }
}
