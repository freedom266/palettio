//
//  DMSerifDisplayFont.swift
//  PalettioUI
//
//  Created by Martin Svoboda on 14.12.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PalettioResources

public enum DMSerifDisplay {
	case regular

	var font: PalettioResourcesFontConvertible {
		switch self {
		case .regular: return PalettioResourcesFontFamily.DMSerifDisplay.regular
		}
	}

	public func font(size: CGFloat) -> UIFont {
		font.font(size: size)
	}
}
