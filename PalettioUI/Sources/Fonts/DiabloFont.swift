//
//  DiabloFont.swift
//  PalettioUI
//
//  Created by Martin Svoboda on 28.12.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//
// https://fonts.adobe.com/fonts/exocet

import SwiftUI
import PalettioResources

public enum Diablo {
	case heavy

	var font: PalettioResourcesFontConvertible {
		switch self {
		case .heavy: return PalettioResourcesFontFamily.Diablo.heavy
		}
	}

	public func font(size: CGFloat) -> UIFont {
		font.font(size: size)
	}
}
