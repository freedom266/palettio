//
//  TextStyles.swift
//  Palettio
//
//  Created by Martin Svoboda on 09.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PalettioResources

public extension View {

    // MARK: Navigation

    /// Navigation Headline
    ///
    /// * Font: `Outfit.semiBold`
    /// * Size: `22`
    /// * Line height: `26`
    func navigationHeadline() -> some View {
        font(Outfit.semiBold.font(size: 22), lineHeight: 26, textStyle: .title)
    }

    /// Navigation Icon
    ///
    /// * Font: `Outfit.medium`
    /// * Size: `20`
    /// * Line height: `24`
    func navigationIcon() -> some View {
        font(Outfit.medium.font(size: 20), lineHeight: 24, textStyle: .title2)
    }

    /// Navigation Text
    ///
    /// * Font: `Outfit.medium`
    /// * Size: `18`
    /// * Line height: `20`
    func navigationText() -> some View {
        font(Outfit.medium.font(size: 18), lineHeight: 20, textStyle: .title3)
    }

    // MARK: Paragraphs

	/// Paragraph Large
	///
	/// * Font: `Outfit.medium`
	/// * Size: `22`
	/// * Line height: `24`
	func paragraphLarge() -> some View {
		font(Outfit.medium.font(size: 22), lineHeight: 24, textStyle: .body)
	}
	
    /// Paragraph Normal
    ///
    /// * Font: `Outfit.regular`
    /// * Size: `15`
    /// * Line height: `21`
    func paragraphNormal() -> some View {
        font(Outfit.regular.font(size: 15), lineHeight: 21, textStyle: .body)
    }
	
	/// Paragraph Normal Bold
	///
	/// * Font: `Outfit.semiBold`
	/// * Size: `16`
	/// * Line height: `20`
	func paragraphNormalBold() -> some View {
		font(Outfit.semiBold.font(size: 16), lineHeight: 20, textStyle: .body)
	}

	// MARK: Captions

	/// Caption Note
	///
	/// * Font: `Outfit.semiBold`
	/// * Size: `12`
	/// * Line height: `16`
	func captionNote(scale: CGFloat = 1) -> some View {
		font(Outfit.semiBold.font(size: 12  * scale), lineHeight: 16 * scale, textStyle: .body)
			.foregroundColor(.basicWhite)
			.padding(.horizontal, 20 * scale)
			.padding(.vertical, 14 * scale)
			.backgroundColor(.spaceBlack.opacity(0.85))
			.cornerRadius(8 * scale)
	}
	
	/// Caption Button
	///
	/// * Font: `Outfit.bold`
	/// * Size: `14`
	/// * Line height: `16`
	func captionButton(scale: CGFloat = 1, primaryColor: ColorItem = .pastelGreen) -> some View {
		font(Outfit.bold.font(size: 14  * scale), lineHeight: 16 * scale, textStyle: .body)
			.foregroundColor(.basicBlack)
			.padding(.vertical, 14 * scale)
			.padding(.horizontal, 16 * scale)
			.frame(minWidth: 50 * scale)
			.backgroundColor(.basicWhite)
			.cornerRadius(8 * scale)
			.padding(.vertical, 4 * scale)
			.padding(.horizontal, 2 * scale)
			.background {
				ColorItem.basicBlack.color
					.cornerRadius(10 * scale)
					.offset(y: 2 * scale)
			}
			.padding(3 * scale)
			.background {
				primaryColor.color
					.cornerRadius(13 * scale)
					.offset(y: 2 * scale)
			}
			.padding(2 * scale)
			.background {
				ColorItem.basicBlack.color
					.cornerRadius(15 * scale)
					.offset(y: 2 * scale)
			}
	}
	
	func captionDiablo(scale: CGFloat = 1) -> some View {
		font(Diablo.heavy.font(size: 16 * scale), lineHeight: 18 * scale, textStyle: .body)
			.foregroundColor(.basicWhite)
			.backgroundColor(.basicBlack.opacity(0.75))
	}
	
	// TODO: Gradient colors
	func captionInstagram(scale: CGFloat = 1) -> some View {
		font(InstagramSansHeadline.regular.font(size: 20 * scale), lineHeight: 22 * scale, textStyle: .body)
			.foregroundColor(.basicBlack)
			.padding(6 * scale)
			.backgroundColor(.basicWhite)
			.cornerRadius(6 * scale)
			.padding(2 * scale)
			.backgroundColor(.basicBlack)
			.cornerRadius(8 * scale)
		
	}
}

struct TextStyle_Previews: PreviewProvider {
    static var previews: some View {
        ScrollView {
            VStack(spacing: 18) {
                Group {
                    Text("Navigation Headline").navigationHeadline()
                    Text("Navigation Icon").navigationIcon()
                    Text("Navigation Text").navigationText()
                }

				Divider()
				
				Group {
					Text("Caption Note").captionNote()
					Text("Caption Button").captionButton()
					Text("Caption Diablo").captionDiablo()
					Text("Caption Instagram").captionInstagram()
				}
				
                Divider()
				
                Group {
					Text("Paragraph Large").paragraphLarge()
					Text("Paragraph Normal").paragraphNormal()
					Text("Paragraph Normal Bold").paragraphNormalBold()
                }
            }
            .frame(maxWidth: .infinity)
			.padding()
        }
    }
}
