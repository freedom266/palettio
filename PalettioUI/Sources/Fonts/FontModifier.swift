//
//  FontModifier.swift
//  Palettio
//
//  Created by Martin Svoboda on 09.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

extension View {
    func font(_ font: UIFont, lineHeight: Double, textStyle: Font.TextStyle?) -> some View {
        let customFont: Font

        // Do not scale font based on dynamic type when
        // nil `relativeTo` specified
        if let textStyle = textStyle {
            customFont = .custom(
                font.fontName,
                size: font.pointSize,
                relativeTo: textStyle
            )
        } else {
            customFont = Font(font)
        }

        return self
            .font(customFont)
            .lineSpacing(lineHeight - font.lineHeight)
            .padding(.vertical, (lineHeight - font.lineHeight) / 2)
    }
}
