//
//  PTNavigationBar.swift
//  PalettioUI
//
//  Created by Martin Svoboda on 07.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

struct PTNavigationBar<Leading: View>: View {
    let title: String
    let back: (() -> Void)?

    var body: some View {
        ZStack(alignment: .leading) {
            Text(title)
                .navigationHeadline()
                .foregroundColor(.accentPrimary)
                .padding(.vertical, 12)
                .frame(maxWidth: .infinity)

            if let back {
                Button(action: back) {
                    Image(systemName: "arrow.left")
                        .navigationIcon()
                        .foregroundColor(.accentPrimary)
                        .padding(.leading, 20)
                }
            }
        }
    }
}

extension PTNavigationBar where Leading == EmptyView {
    init(
        title: String,
        back: (() -> Void)? = nil
    ) {
        self.title = title
        self.back = back
    }
}

#Preview {
    PTNavigationBar(title: "Title", back: {})
}

#Preview {
    PTNavigationBar(title: "Title")
}
