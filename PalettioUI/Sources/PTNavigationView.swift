//
//  PTNavigationView.swift
//  Palettio
//
//  Created by Martin Svoboda on 07.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

public struct PTNavigationView<Content: View, Trailing: View>: View {
    let title: String
    let back: (() -> Void)?
    let content: () -> Content
    let trailing: () -> Trailing

    public init(
        title: String,
        back: (() -> Void)? = nil,
        @ViewBuilder content: @escaping () -> Content,
        @ViewBuilder trailing: @escaping () -> Trailing
    ) {
        self.title = title
        self.back = back
        self.content = content
        self.trailing = trailing
    }

    public var body: some View {
        VStack(spacing: 0) {
            ZStack(alignment: .trailing) {
                PTNavigationBar(title: title, back: back)
                
                trailing()
                    .padding(.trailing, 20)
            }

            content()
        }
    }
}

extension PTNavigationView where Trailing == EmptyView {
    public init(
        title: String,
        back: (() -> Void)? = nil,
        content: @escaping () -> Content
    ) {
        self.title = title
        self.back = back
        self.content = content
        self.trailing = { EmptyView() }
    }
}

struct PTNavigationView_Previews: PreviewProvider {
    static var previews: some View {
        // swiftlint:disable:next multiple_closures_with_trailing_closure
        PTNavigationView(title: "Title", back: {}) {
            ScrollView {
                Text("Hello world")
                    .padding(.top, 100)
            }
        } trailing: {
            Image(systemName: "square.and.arrow.up")
                .navigationIcon()
                .foregroundColor(.accentPrimary)
        }

        PTNavigationView(title: "Title") {
            ScrollView {
                Text("Hello world")
                    .padding(.top, 100)
            }
        }
    }
}
