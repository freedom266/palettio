//
//  PTDestructiveAlert.swift
//  PalettioUI
//
//  Created by Martin Svoboda on 15.12.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PalettioResources

public struct PTDestructiveAlert: View {
	@Binding var isPresented: Bool
	
	let title: String
	let message: String
	let primaryAction: () -> Void
	let secondaryAction: (() -> Void)?
	
	public init(
		isPresented: Binding<Bool>,
		title: String,
		message: String,
		primaryAction: @escaping () -> Void,
		secondaryAction: (() -> Void)? = nil
	) {
		self._isPresented = isPresented
		self.title = title
		self.message = message
		self.primaryAction = primaryAction
		self.secondaryAction = secondaryAction
	}
	
	public var body: some View {
		VStack(spacing: 0) {
			VStack(spacing: 8) {
				Text(title)
					.paragraphLarge()
					.foregroundColor(.foregroundsPrimary)
				
				Text(message)
					.paragraphNormal()
					.foregroundColor(.foregroundsPrimary)
					.multilineTextAlignment(.center)
					.padding(.horizontal)
			}
			.padding(.vertical, 16)
			
			Rectangle()
				.frame(height: 1)
				.foregroundColor(.foregroundsSecondary)
				.opacity(0.2)
			
			HStack {
				Button {
					secondaryAction?()
					isPresented = false
				} label: {
					Text(L10n.General.no)
						.paragraphNormalBold()
						.foregroundColor(.blue)
						.frame(maxWidth: .infinity)
				}
				
				Button {
					primaryAction()
					isPresented = false
				} label: {
					Text(L10n.General.yes)
						.paragraphNormal()
						.foregroundColor(.red)
						.frame(maxWidth: .infinity)
				}
			}
			.padding(.vertical, 10)
			.overlay {
				Rectangle()
					.frame(width: 1)
					.foregroundColor(.foregroundsSecondary)
					.opacity(0.25)
			}
		}
		.frame(width: 250)
		.backgroundColor(.backgroundsPrimary)
		.cornerRadius(3)
		.frame(
			maxWidth: .infinity,
			maxHeight: .infinity
		)
		.background {
			Color.black.opacity(0.25)
				.ignoresSafeArea()
		}
	}
}

#Preview {
	PTDestructiveAlert(
		isPresented: .constant(true),
		title: "Delete photos",
		message: "Are you sure you want to delete these photos?",
		primaryAction: { },
		secondaryAction: { }
	)
}
