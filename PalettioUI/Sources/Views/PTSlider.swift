//
//  PTSlider.swift
//  PalettioUI
//
//  Created by Martin Svoboda on 29.11.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

struct PTSlider: View {
	@Binding var value: Double
	var maxValue: Double
	var dragPointSize: CGFloat = 24
	
	@State var prevDragPointLocation: CGFloat?
	var dragPointLocation: CGFloat {
		value * sliderWidth
	}
	
	@State var sliderWidth: CGFloat = 0
	@State var gestureOffset: CGFloat = 0
	
	init(value: Binding<Double>, maxValue: CGFloat = 1) {
		self._value = value
		self.maxValue = maxValue

		if value.wrappedValue > maxValue {
			self.value = maxValue
		}
	}
	
	var body: some View {
		ZStack(alignment: .leading) {
			lowerSlider
			
			upperSlider
			
			dragPoint
			
			dragValue
		}
		.padding(.top, 32)
	}
	
	private var lowerSlider: some View {
		Capsule()
			.fill(.backgroundsTertiary)
			.frame(height: 5)
			.overlay {
				GeometryReader { proxy in
					Color.clear
						.onAppear {
							sliderWidth = proxy.frame(in: .global).size.width
							prevDragPointLocation = dragPointLocation
						}
				}
			}
	}
	
	private var upperSlider: some View {
		Capsule()
			.fill(.foregroundsSecondary)
			.frame(height: 4)
			.frame(width: dragPointLocation)
	}
	
	private var dragPoint: some View {
		Capsule()
			.fill(.backgroundsSecondary)
			.frame(width: dragPointSize, height: dragPointSize * 0.85)
			.background {
				RoundedRectangle(cornerRadius: dragPointSize / 2)
					.stroke(.foregroundsPrimary, lineWidth: 3)
			}
			.offset(x: dragPointLocation - dragPointSize / 2)
			.gesture(
				DragGesture(minimumDistance: 0)
					.onChanged {
						if let prevDragPointLocation {
							gestureOffset = $0.location.x - prevDragPointLocation
							self.prevDragPointLocation = nil
						}
						
						value = min(1, max(0, ($0.location.x - gestureOffset) / sliderWidth))
					}
					.onEnded { _ in
						prevDragPointLocation = dragPointLocation
					}
			)
			.simultaneousGesture(
				TapGesture(count: 2).onEnded {
					value = maxValue / 2
				}
			)
	}
	
	private var dragValue: some View {
		Text(String(format: "%.0f", value * 100))
			.paragraphNormal()
			.foregroundColor(.accentPrimary)
			.frame(width: 50)
			.offset(
				x: dragPointLocation - 50 / 2,
				y: -25
			)
	}
}

#Preview {
	VStack(spacing: 50) {
		PTSlider(value: .constant(0.25))
		
		PTSlider(value: .constant(0.50))
		
		PTSlider(value: .constant(0.75))
	}
	.padding(30)
	.backgroundColor(.backgroundsPrimary)
}
