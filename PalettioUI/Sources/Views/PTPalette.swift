//
//  PTPalette.swift
//  PalettioUI
//
//  Created by Martin Svoboda on 13.10.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PalettioCore
import PalettioResources

public struct PTPalette: View {
	@Binding var colorA: Color
	@Binding var colorB: Color
	@Binding var opacity: Double
	@Binding var angle: Double
	
	@State var isAngleSetterActive = false
	
	var angleSetterSize: CGFloat {
		isAngleSetterActive ? 2.25 : 1
	}
	
	public init(
		gradientOverlay: Binding<GradientOverlay>
	) {
		self._colorA = gradientOverlay.colorA
		self._colorB = gradientOverlay.colorB
		self._opacity = gradientOverlay.opacity
		self._angle = gradientOverlay.angle
	}
	
	public var body: some View {
		VStack(spacing: 0) {
			PTSlider(value: $opacity)
				.padding(.horizontal, 4)
			
			HStack {
				colorPicker(color: $colorA)
				
				Spacer()
				
				colorPicker(color: $colorB)
			}
			.padding(12)
			.background {
				LinearGradient(
					colors: [
						ColorItem.backgroundsPrimary.color,
						ColorItem.backgroundsSecondary.color,
						ColorItem.backgroundsPrimary.color
					],
					startPoint: .leading,
					endPoint: .trailing
				)
			}
			.cornerRadius(50)
			.background {
				RoundedRectangle(cornerRadius: 50)
					.stroke(.backgroundsTertiary.opacity(0.75), lineWidth: 2)
			}
			.padding(.vertical, 20)
			.overlay(angleSetter)
		}
		.padding(.horizontal, 64)
		.frame(maxWidth: 500)
	}
	
	private var angleSetter: some View {
		Circle()
			.fill(
				LinearGradient(
					colors: [
						colorA.opacity(10), 
						colorB.opacity(10)
					],
					startPoint: .init(x: 0.5, y: 1),
					endPoint: .init(x: 0.5, y: 0)
				)
			)
			.frame(width: 33 * angleSetterSize)
			.background {
				Circle()
					.fill(ColorItem.basicBlack)
			}
			.overlay {
				Circle()
					.fill(.backgroundsSecondary)
					.frame(width: 30 * angleSetterSize)
			}
			.overlay(alignment: .top) {
				Circle()
					.fill(
						LinearGradient(
							colors: [
								colorA.opacity(10),
								colorB.opacity(10)
							],
							startPoint: .init(x: 0.5, y: 2),
							endPoint: .init(x: 0.5, y: 0.75)
						)
					)
					.frame(width: 9 * sqrt(angleSetterSize))
			}
			.rotationEffect(.degrees(angle))
			.background {
				// No change for UI but the area for gesture is bigger now
				ColorItem.backgroundsPrimary.color
					.frame(width: 59, height: 54)
					.opacity(0.02)
			}
			.gesture(
				DragGesture(minimumDistance: 0)
					.onChanged {
						angle = $0.translation.angle
						
						if !isAngleSetterActive {
							withAnimation {
								isAngleSetterActive = true
							}
						}
					}
					.onEnded { _ in
						withAnimation {
							isAngleSetterActive = false
						}
					}
			)
	}
	
	private func colorPicker(color: Binding<Color>) -> some View {
		Circle()
			.fill(.foregroundsSecondary)
			.frame(width: 33)
			.overlay {
				Circle()
					.fill(.backgroundsPrimary)
					.frame(width: 30)

				Circle()
					.trim(from: 0, to: 0.5)
					.fill(.basicBlack)
					.frame(width: 24)
				
				Circle()
					.trim(from: 0.5, to: 1)
					.fill(.basicWhite)
					.frame(width: 24)
				
				Circle()
					.fill(color.wrappedValue)
					.frame(width: 24)
				
				ColorPicker("", selection: color, supportsOpacity: true)
					.scaleEffect(1.5)
					.labelsHidden()
					.opacity(0.02)
			}
			.rotationEffect(.degrees(120))
	}
}

#Preview {
	PTPalette(
		gradientOverlay: .constant(.init())
	)
}
