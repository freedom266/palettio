//
//  PTColorPicker.swift
//  PalettioUI
//
//  Created by Martin Svoboda on 17.12.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PalettioResources

public struct PTColorPicker: View {
	@Binding var selectedColor: ColorItem
	
	let action: (ColorItem) -> Void
	
	var allColors: [ColorItem] {
		[
			.pastelBlue,
			.pastelPink,
			.pastelGreen,
			.pastelYellow,
			.pastelRed
		]
	}
	
	public init(
		selectedColor: Binding<ColorItem>,
		action: @escaping (ColorItem) -> Void
	) {
		self._selectedColor = selectedColor
		self.action = action
	}
	
	public var body: some View {
		VStack {
			HStack(spacing: 0) {
				ForEach(allColors, id: \.color) { color in
					colorButton(color)
				}
			}
		}
		.padding(.vertical)
	}
	
	private func colorButton(_ color: ColorItem) -> some View {
		Button {
			action(color)
		} label: {
			ZStack {
				Circle()
					.fill(.white)
					.frame(width: 35)
					.opacity(color == selectedColor ? 1 : 0)
				
				Circle()
					.fill(.basicBlack)
					.frame(width: 30)
				
				Circle()
					.fill(color)
					.frame(width: 25)
				
				Circle()
					.fill(color)
					.frame(width: 20)
			}
			.frame(width: 55, height: 55)
		}
	}
}

#Preview {
	VStack(spacing: 50) {
		PTColorPicker(selectedColor: .constant(.pastelPink)) { _ in }
		
		PTColorPicker(selectedColor: .constant(.pastelGreen)) { _ in }
	}
	.padding()
	.backgroundColor(.backgroundsPrimary)
}
