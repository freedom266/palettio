//
//  PTNotification.swift
//  PalettioUI
//
//  Created by Martin Svoboda on 11.10.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PalettioCore

public struct PTNotification: View {
	@Binding var content: NotificationContent
	
	public init(content: Binding<NotificationContent>) {
		self._content = content
	}
	
	public var body: some View {
		HStack(spacing: 12) {
			ZStack {
				ProgressView()
					.opacity(content.type.isLoading ? 1 : 0)
				
				Image(systemName: "checkmark.circle.fill")
					.foregroundColor(.successPrimary)
					.opacity(content.type.isSuccess ? 1 : 0)
				
				Image(systemName: "xmark.circle.fill")
					.foregroundColor(.failPrimary)
					.opacity(content.type.isFail ? 1 : 0)
			}
			
			Text(content.message)
				.lineLimit(1)
				.paragraphNormal()
				.foregroundColor(.foregroundsPrimary)
				.frame(maxWidth: .infinity)
		}
		.frame(width: 200)
		.padding(16)
		.backgroundColor(.backgroundsSecondary)
		.cornerRadius(30)
		.zIndex(1)
		.shadow(color: .init(hexString: "000000").opacity(0.16), radius: 6, x: 0, y: 2)
		.transition(.move(edge: .top))
		.padding(.bottom, 50)
	}
}

#Preview {
	VStack(spacing: 40) {
		PTNotification(content: .constant(.init(type: .success("Saved to Camera Roll"))))
		
		PTNotification(content: .constant(.init(type: .fail("Something went wrong"))))
		
		PTNotification(content: .constant(.init(type: .loading("Photo is exporting"))))
	}
	.padding()
	.backgroundColor(.backgroundsPrimary)
}
