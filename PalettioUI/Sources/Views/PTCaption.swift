//
//  PTCaption.swift
//  Develop
//
//  Created by Martin Svoboda on 12.10.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PalettioCore

public struct PTCaption: View {
	let data: CaptionData
	let scale: CGFloat

	// MARK: Inits
	
	public init(data: CaptionData, scale: CGFloat = 1) {
		self.data = data
		self.scale = scale
	}
	
	public var body: some View {
		Text(data.text)
			.captionButton(
				scale: data.globalScale * scale,
				primaryColor: data.color.primary
			)
			.padding()
	}
}

#Preview {
	VStack(spacing: 40) {
		PTCaption(data: .init(text: "Lorem ipsum", transform: .init()), scale: 0.5)
		PTCaption(data: .init(text: "Lorem ipsum", transform: .init()), scale: 1.0)
		PTCaption(data: .init(text: "Lorem ipsum", transform: .init()), scale: 2.0)
	}
	.padding(40)
	.backgroundColor(.backgroundsPrimary)
}
