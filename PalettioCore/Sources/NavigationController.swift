//
//  NavigationController.swift
//  PalettioCore
//
//  Created by Martin Svoboda on 06.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import UIKit

public final class NavigationController: UINavigationController, UIGestureRecognizerDelegate {
    /// Custom back buttons disable the interactive pop animation
    /// To enable it back we set the recognizer to `self`
    override public func viewDidLoad() {
        super.viewDidLoad()

        interactivePopGestureRecognizer?.delegate = self
    }

    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        viewControllers.count > 1
    }
}
