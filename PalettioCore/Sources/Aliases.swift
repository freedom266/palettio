import UIKit
import ACKategoriesCore
import ACKategories_iOS

public typealias BaseViewModel = Base.ViewModel
public typealias BaseFlowCoordinator = Base.FlowCoordinatorNoDeepLink
