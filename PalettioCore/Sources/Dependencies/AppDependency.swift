//
//  AppDependency.swift
//  Palettio
//
//  Created by Martin Svoboda on 07.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import Foundation

public final class AppDependency: ObservableObject {
	public let photoItemsRepository: PhotoItemsRepository = .init()
	public let editToolsProvider: EditToolsProvider = .init()
	public let previewSizeProvider: PreviewSizeProvider = .init()
}

public let dependencies = AppDependency()
