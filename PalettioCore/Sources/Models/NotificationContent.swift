//
//  NotificationContent.swift
//  PalettioCore
//
//  Created by Martin Svoboda on 11.10.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import Foundation
import PalettioResources

public struct NotificationContent {
	public let type: NotificationType
	
	public var message: String {
		switch type {
		case .fail(let message): message
		case .success(let message): message
		case .loading(let message): message
		case .none: ""
		}
	}
	
	public init(type: NotificationType = .none) {
		self.type = type
	}
}
