//
//  NotificationType.swift
//  PalettioCore
//
//  Created by Martin Svoboda on 30.12.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import Foundation

public enum NotificationType {
	case fail(String)
	case success(String)
	case loading(String)
	case none
	
	public var isFail: Bool {
		switch self {
		case .fail: true
		default: false
		}
	}

	public var isSuccess: Bool {
		switch self {
		case .success: true
		default: false
		}
	}
	
	public var isLoading: Bool {
		switch self {
		case .loading: true
		default: false
		}
	}
}
