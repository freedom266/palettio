//
//  CaptionTransform.swift
//  PalettioCore
//
//  Created by Martin Svoboda on 06.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

public struct CaptionTransform: Codable {
	var previewSize: CGSize
	
	public var isLockedHorizontaly = false
	public var isLockedVerticaly = false
	public var isLockedByRotation = false

	public var currentLocation: CGSize = .zero
	public var currentRotation: CGFloat = 0
	public var relativeScale: CGFloat = 1
	
	var lastLocation: CGSize = .zero
	var lastRotation: CGFloat = 0
	var globalScale: CGFloat = 1
	
	public var distanceFromTrash: CGFloat { currentLocation.distance(to: trashLocation) }
	private var trashLocation: CGSize { .trashLocation(size: previewSize) }

	private var previousLocation: CGSize = .zero
	private var previousRotation: CGFloat = .zero
	
	// MARK: Init
	
	public init(previewSize: CGSize = .zero) { 
		self.previewSize = previewSize
	}
}

public extension CaptionTransform {
	mutating func restorePreviousTransform() {
		currentLocation = lastLocation
		currentRotation = lastRotation
		relativeScale = 1
	}
	
	mutating func prepareForMove() {
		currentLocation = .zero
		currentRotation = 0
		relativeScale = 1 / globalScale
	}
	
	mutating func updateScaleBasedOnTrash(_ isTrashOpen: Bool) {
		if isTrashOpen {
			if relativeScale == 1 {
				relativeScale = 0.5 / globalScale
			}
		} else {
			if relativeScale != 1 {
				relativeScale = 1
			}
		}
	}
}

public extension CaptionTransform {
	mutating func updateCurrentLocation(offset: CGSize) {
		previousLocation = currentLocation
		currentLocation = safeLocation(offset: offset)
		
		updateLocationLocks()
		
		if isLockedHorizontaly { currentLocation.width = 0 }
		if isLockedVerticaly { currentLocation.height = 0 }
	}
	
	mutating func updateCurrentRotation(degrees: CGFloat) {
		let newAngle = lastRotation + degrees
		let rotation = newAngle.inRangePlusMinus180
		let rightAngle = rotation.roundToNearestRightAngle
		let angleDeviation = abs(rotation - rightAngle)
		
		previousRotation = currentRotation
		let velocity = abs(previousRotation - rotation)
		
		if isLockedByRotation {
			if angleDeviation >= .thresholdForSnapToRotRuler {
				isLockedByRotation = false
			}
		} else if velocity < .rotVelocityThreshold {
			if angleDeviation < .thresholdForSnapToRotRuler {
				isLockedByRotation = true
			}
		}
		
		currentRotation = isLockedByRotation ? rightAngle : rotation
	}
}

private extension CaptionTransform {
	mutating func updateLocationLocks() {
		if isLockedHorizontaly {
			if shouldBeUnlocked(newValue: currentLocation.width) {
				isLockedHorizontaly = false
			}
		} else if shouldBeLockedToRuler(
			oldValue: previousLocation.width,
			newValue: currentLocation.width
		) {
			isLockedHorizontaly = true
		}
		
		if isLockedVerticaly {
			if shouldBeUnlocked(newValue: currentLocation.height) {
				isLockedVerticaly = false
			}
		} else {
			if shouldBeLockedToRuler(
				oldValue: previousLocation.height,
				newValue: currentLocation.height
			) {
				isLockedVerticaly = true
			}
		}
	}
}

private extension CaptionTransform {
	func safeLocation(offset: CGSize) -> CGSize {
		let currentWidth = lastLocation.width + offset.width
		let currentHeight = lastLocation.height + offset.height

		let safeWidth = min(previewSize.width / 2, max(currentWidth, -previewSize.width / 2))
		let safeHeight = min(previewSize.height / 2, max(currentHeight, -previewSize.height / 2))

		return .init(width: safeWidth, height: safeHeight)
	}
	
	func shouldBeLockedToRuler(oldValue: CGFloat, newValue: CGFloat) -> Bool {
		let velocity = abs(oldValue - newValue)
		let wasDirectionPositive = oldValue.isPositive
		let isDirectionPositive = newValue.isPositive
		let isItOnEdge = wasDirectionPositive != isDirectionPositive
		
		return isItOnEdge && velocity < .locVelocityThreshold
	}
	
	func shouldBeUnlocked(newValue: CGFloat) -> Bool {
		abs(newValue) > .thresholdForSnapToLocRuler
	}
}
