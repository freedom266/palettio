//
//  CaptionColors.swift
//  PalettioCore
//
//  Created by Martin Svoboda on 17.12.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PalettioResources

public struct CaptionColors: Codable {
	public var primary: ColorItem
	
	public init(primary: ColorItem = .pastelGreen) {
		self.primary = primary
	}
}
