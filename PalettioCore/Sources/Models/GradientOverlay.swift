//
//  GradientOverlay.swift
//  PalettioCore
//
//  Created by Martin Svoboda on 29.11.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

public struct GradientOverlay: Codable {
	public var colorA: Color
	public var colorB: Color
	public var opacity: Double
	public var angle: Double
	
	// MARK: Init
	
	public init(
		colorA: Color = .white.opacity(0.5),
		colorB: Color = .black.opacity(0.5),
		opacity: Double = 0.5,
		angle: Double = 0
	) {
		self.colorA = colorA
		self.colorB = colorB
		self.opacity = opacity
		self.angle = angle
	}
}

extension GradientOverlay {
	public var view: some View {
		LinearGradient(
			colors: [
				colorA.opacity(opacity),
				colorB.opacity(opacity)
			],
			startPoint: startPoint,
			endPoint: endPoint
		)
	}
	
	private var startPoint: UnitPoint {
		let distanceFromX = abs(180 - (angle + 90).truncatingRemainder(dividingBy: 360))
		let x = distanceFromX / 180
		
		let distanceFromY = abs(180 - angle.truncatingRemainder(dividingBy: 360))
		let y = distanceFromY / 180
		return .init(x: x, y: y)
	}
	
	private var endPoint: UnitPoint {
		.init(
			x: 1 - startPoint.x,
			y: 1 - startPoint.y
		)
	}
}
