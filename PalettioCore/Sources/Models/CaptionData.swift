//
//  CaptionData.swift
//  PalettioCore
//
//  Created by Martin Svoboda on 14.10.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

public struct CaptionData: Identifiable, Codable {
	public var id: String
	public var text: String
	public var transform: CaptionTransform
	public var color: CaptionColors
	
	// MARK: Init
	
	public init(
		id: String = UUID().uuidString,
		text: String = "",
		transform: CaptionTransform = .init(),
		color: CaptionColors = .init()
	) {
		self.id = id
		self.text = text
		self.transform = transform
		self.color = color
	}
}

public extension CaptionData {
	var currentLocation: CGSize { transform.currentLocation }
	var currentRotation: CGFloat { transform.currentRotation }
	
	var relativeScale: CGFloat { transform.relativeScale }
	var globalScale: CGFloat { transform.globalScale }
	
	var isLockedHorizontaly: Bool { transform.isLockedHorizontaly }
	var isLockedVerticaly: Bool { transform.isLockedVerticaly }
	var isLockedByRotation: Bool { transform.isLockedByRotation }
	
	var isLocationOrigin: Bool {
		transform.currentLocation == .zero &&
		transform.currentRotation == 0 &&
		transform.globalScale == 1
	}
}

public extension CaptionData {
	mutating func updateCurrentLocation(offset: CGSize) {
		transform.updateCurrentLocation(offset: offset)
	}
	
	mutating func updateLastLocation() {
		transform.lastLocation = transform.currentLocation
	}
	
	mutating func updateCurrentRotation(degrees: CGFloat) {
		transform.updateCurrentRotation(degrees: degrees)
	}
	
	mutating func updateLastRotation() {
		transform.lastRotation = transform.currentRotation
	}
	
	mutating func updateRelativeScale(_ newScale: CGFloat) {
		guard globalScale * newScale < .maxCaptionScale else { return }
		transform.relativeScale = newScale
	}
	
	mutating func updateGlobalScale() {
		transform.globalScale *= relativeScale
		transform.relativeScale = 1
	}
	
	mutating func setPreviewSize(size: CGSize) {
		transform.previewSize = size
	}
	
	mutating func clear() {
		self = .init(
			transform: .init(
				previewSize: self.transform.previewSize
			)
		)
	}
}

public extension CaptionData {
	mutating func updateScaleBasedOnTrash(_ isTrashOpen: Bool) {
		transform.updateScaleBasedOnTrash(isTrashOpen)
	}
	
	mutating func restorePreviousTransform() {
		transform.restorePreviousTransform()
	}
	
	mutating func prepareForMove() {
		transform.prepareForMove()
	}
}
