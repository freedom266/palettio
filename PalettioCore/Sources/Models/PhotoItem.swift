//
//  PhotoItem.swift
//  PalettioCore
//
//  Created by Martin Svoboda on 07.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import Combine

public final class PhotoItem: Identifiable {
	public var url: URL
	
	public lazy var image: UIImage = {
		UIImage(contentsOfFile: imageUrl.path) ?? UIImage()
	}()
	
	public lazy var thumbnail: UIImage? = {
		UIImage(contentsOfFile: thumbnailUrl.path)
	}()
	
	public var thumbnailPublisher = PassthroughSubject<UIImage?, Never>()
	
	// MARK: Init
	
	public init(url: URL) {
		self.url = url
	}
}

public extension PhotoItem {
	var overlay: GradientOverlay {
		guard
			let data = try? Data(contentsOf: overlayUrl),
			let overlay = try? JSONDecoder().decode(GradientOverlay.self, from: data)
		else { return .init() }
		
		return overlay
	}
	
	var captions: [CaptionData] {
		guard
			let data = try? Data(contentsOf: captionsUrl),
			let captions = try? JSONDecoder().decode([CaptionData].self, from: data)
		else { return [] }
		
		return captions
	}
}

public extension PhotoItem {
	func update(thumbnail: UIImage) {
		let thumbnail = thumbnail.croppedToSquare
		thumbnailPublisher.send(thumbnail)
		
		let data = thumbnail.pngData()
		FileManager.default.saveData(data: data, dataUrl: thumbnailUrl)
	}
	
	func update(overlay: GradientOverlay) async {
		let data = try? JSONEncoder().encode(overlay)
		FileManager.default.saveData(data: data, dataUrl: overlayUrl)
	}
	
	func update(captions: [CaptionData]) async {
		let data = try? JSONEncoder().encode(captions)
		FileManager.default.saveData(data: data, dataUrl: captionsUrl)
	}
}

extension PhotoItem {
	var imageUrl: URL {
		url.appendingPathComponent(PhotoItem.imagePath)
	}
	
	var thumbnailUrl: URL {
		url.appendingPathComponent(PhotoItem.thumbnailPath)
	}
	
	var overlayUrl: URL {
		url.appendingPathComponent(PhotoItem.overlayPath)
	}
	
	var captionsUrl: URL {
		url.appendingPathComponent(PhotoItem.captionsPath)
	}

	static var imagePath: String {
		"image"
	}
	
	static var thumbnailPath: String {
		"thumbnail"
	}
	
	static var overlayPath: String {
		"overlay"
	}
	
	static var captionsPath: String {
		"captions"
	}
}

extension PhotoItem: Hashable {
	public static func == (lhs: PhotoItem, rhs: PhotoItem) -> Bool {
		lhs.id == rhs.id
	}
	
	public func hash(into hasher: inout Hasher) {
		hasher.combine(id)
	}
}
