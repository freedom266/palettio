//
//  PhotoItemMock.swift
//  Palettio
//
//  Created by Martin Svoboda on 23.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

public extension [PhotoItem] {
    static func mock() -> [PhotoItem] {
        [
            .mock1(),
            .mock2(),
            .mock3(),
            .mock4(),
            .mock5(),
            .mock6(),
            .mock7(),
            .mock8(),
            .mock9(),
			.mock10(),
			.mock11(),
			.mock12(),
			.mock13(),
			.mock14(),
			.mock15(),
			.mock16(),
			.mock17(),
			.mock18(),
			.mock19(),
			.mock20(),
			.mock21(),
			.mock22(),
			.mock23()
        ]
    }
}

fileprivate extension PhotoItem {
	convenience init(imageUrl: URL) {
		self.init(url: imageUrl)
		
		let data = try? Data(contentsOf: imageUrl)
		// swiftlint:disable:next force_unwrapping
		self.thumbnail = UIImage(data: data!)!
		// swiftlint:disable:next force_unwrapping
		self.image = UIImage(data: data!)!
	}
}

public extension PhotoItem {
    static func mock1() -> PhotoItem {
        // swiftlint:disable:next force_unwrapping
        let imageUrl = URL(string: PhotoItem.baseUrl + PhotoItem.urlStringMock1)!
        return .init(imageUrl: imageUrl)
    }

    static func mock2() -> PhotoItem {
        // swiftlint:disable:next force_unwrapping
        let imageUrl = URL(string: PhotoItem.baseUrl + PhotoItem.urlStringMock2)!
        return .init(imageUrl: imageUrl)
    }

    static func mock3() -> PhotoItem {
        // swiftlint:disable:next force_unwrapping
        let imageUrl = URL(string: PhotoItem.baseUrl + PhotoItem.urlStringMock3)!
        return .init(imageUrl: imageUrl)
    }

    static func mock4() -> PhotoItem {
        // swiftlint:disable:next force_unwrapping
        let imageUrl = URL(string: PhotoItem.baseUrl + PhotoItem.urlStringMock4)!
        return .init(imageUrl: imageUrl)
    }

    static func mock5() -> PhotoItem {
        // swiftlint:disable:next force_unwrapping
        let imageUrl = URL(string: PhotoItem.baseUrl + PhotoItem.urlStringMock5)!
        return .init(imageUrl: imageUrl)
    }

    static func mock6() -> PhotoItem {
        // swiftlint:disable:next force_unwrapping
        let imageUrl = URL(string: PhotoItem.baseUrl + PhotoItem.urlStringMock6)!
        return .init(imageUrl: imageUrl)
    }

    static func mock7() -> PhotoItem {
        // swiftlint:disable:next force_unwrapping
        let imageUrl = URL(string: PhotoItem.baseUrl + PhotoItem.urlStringMock7)!
        return .init(imageUrl: imageUrl)
    }

    static func mock8() -> PhotoItem {
        // swiftlint:disable:next force_unwrapping
        let imageUrl = URL(string: PhotoItem.baseUrl + PhotoItem.urlStringMock8)!
        return .init(imageUrl: imageUrl)
    }

    static func mock9() -> PhotoItem {
        // swiftlint:disable:next force_unwrapping
        let imageUrl = URL(string: PhotoItem.baseUrl + PhotoItem.urlStringMock9)!
        return .init(imageUrl: imageUrl)
    }
	
	
	static func mock10() -> PhotoItem {
		// swiftlint:disable:next force_unwrapping
		let imageUrl = URL(string: PhotoItem.baseUrl + PhotoItem.urlStringMock10)!
		return .init(imageUrl: imageUrl)
	}
	
	static func mock11() -> PhotoItem {
		// swiftlint:disable:next force_unwrapping
		let imageUrl = URL(string: PhotoItem.baseUrl + PhotoItem.urlStringMock11)!
		return .init(imageUrl: imageUrl)
	}

	static func mock12() -> PhotoItem {
		// swiftlint:disable:next force_unwrapping
		let imageUrl = URL(string: PhotoItem.baseUrl + PhotoItem.urlStringMock12)!
		return .init(imageUrl: imageUrl)
	}

	static func mock13() -> PhotoItem {
		// swiftlint:disable:next force_unwrapping
		let imageUrl = URL(string: PhotoItem.baseUrl + PhotoItem.urlStringMock13)!
		return .init(imageUrl: imageUrl)
	}

	static func mock14() -> PhotoItem {
		// swiftlint:disable:next force_unwrapping
		let imageUrl = URL(string: PhotoItem.baseUrl + PhotoItem.urlStringMock14)!
		return .init(imageUrl: imageUrl)
	}

	static func mock15() -> PhotoItem {
		// swiftlint:disable:next force_unwrapping
		let imageUrl = URL(string: PhotoItem.baseUrl + PhotoItem.urlStringMock15)!
		return .init(imageUrl: imageUrl)
	}

	static func mock16() -> PhotoItem {
		// swiftlint:disable:next force_unwrapping
		let imageUrl = URL(string: PhotoItem.baseUrl + PhotoItem.urlStringMock16)!
		return .init(imageUrl: imageUrl)
	}

	static func mock17() -> PhotoItem {
		// swiftlint:disable:next force_unwrapping
		let imageUrl = URL(string: PhotoItem.baseUrl + PhotoItem.urlStringMock17)!
		return .init(imageUrl: imageUrl)
	}

	static func mock18() -> PhotoItem {
		// swiftlint:disable:next force_unwrapping
		let imageUrl = URL(string: PhotoItem.baseUrl + PhotoItem.urlStringMock18)!
		return .init(imageUrl: imageUrl)
	}

	static func mock19() -> PhotoItem {
		// swiftlint:disable:next force_unwrapping
		let imageUrl = URL(string: PhotoItem.baseUrl + PhotoItem.urlStringMock19)!
		return .init(imageUrl: imageUrl)
	}
	
	static func mock20() -> PhotoItem {
		// swiftlint:disable:next force_unwrapping
		let imageUrl = URL(string: PhotoItem.baseUrl + PhotoItem.urlStringMock20)!
		return .init(imageUrl: imageUrl)
	}
	
	static func mock21() -> PhotoItem {
		// swiftlint:disable:next force_unwrapping
		let imageUrl = URL(string: PhotoItem.baseUrl + PhotoItem.urlStringMock21)!
		return .init(imageUrl: imageUrl)
	}
	
	static func mock22() -> PhotoItem {
		// swiftlint:disable:next force_unwrapping
		let imageUrl = URL(string: PhotoItem.baseUrl + PhotoItem.urlStringMock22)!
		return .init(imageUrl: imageUrl)
	}
	
	static func mock23() -> PhotoItem {
		// swiftlint:disable:next force_unwrapping
		let imageUrl = URL(string: PhotoItem.baseUrl + PhotoItem.urlStringMock23)!
		return .init(imageUrl: imageUrl)
	}
}

fileprivate extension PhotoItem {
    static var baseUrl: String { "https://burst.shopifycdn.com/photos/" }

    static var urlStringMock1: String {
        // swiftlint:disable:next line_length
        "green-plants-grow-in-lines-on-rolling-hills.jpg?width=925&format=pjpg&exif=0&iptc=0%201x,%20https://burst.shopifycdn.com/photos/green-plants-grow-in-lines-on-rolling-hills.jpg"
    }

    static var urlStringMock2: String {
        // swiftlint:disable:next line_length
        "person-stands-on-rocks-poking-out-of-the-ocean-shoreline.jpg?width=925&format=pjpg&exif=0&iptc=0%201x,%20https://burst.shopifycdn.com/photos/person-stands-on-rocks-poking-out-of-the-ocean-shoreline.jpg"
    }

    static var urlStringMock3: String {
        // swiftlint:disable:next line_length
        "flatlay-of-jade-face-roller-laying-on-a-grey-stone.jpg?width=925&format=pjpg&exif=0&iptc=0%201x,%20https://burst.shopifycdn.com/photos/flatlay-of-jade-face-roller-laying-on-a-grey-stone.jpg"
    }

    static var urlStringMock4: String {
        // swiftlint:disable:next line_length
        "people-looking-at-light-beams-from-the-walls.jpg?width=925&format=pjpg&exif=0&iptc=0%201x,%20https://burst.shopifycdn.com/photos/people-looking-at-light-beams-from-the-walls.jpg"
    }

    static var urlStringMock5: String {
        // swiftlint:disable:next line_length
        "people-sit-outside-building-lit-with-red-light.jpg?width=925&format=pjpg&exif=0&iptc=0%201x,%20https://burst.shopifycdn.com/photos/people-sit-outside-building-lit-with-red-light.jpg"
    }

    static var urlStringMock6: String {
        // swiftlint:disable:next line_length
        "mobile-phone-and-gimbal-in-hand.jpg?width=925&format=pjpg&exif=0&iptc=0%201x,%20https://burst.shopifycdn.com/photos/mobile-phone-and-gimbal-in-hand.jpg"
    }

    static var urlStringMock7: String {
        // swiftlint:disable:next line_length
        "side-view-colorful-clothing-on-hangers.jpg?width=925&format=pjpg&exif=0&iptc=0%201x,%20https://burst.shopifycdn.com/photos/side-view-colorful-clothing-on-hangers.jpg"
    }

    static var urlStringMock8: String {
        // swiftlint:disable:next line_length
        "person-in-black-holds-camera-at-their-waist.jpg?width=925&format=pjpg&exif=0&iptc=0%201x,%20https://burst.shopifycdn.com/photos/person-in-black-holds-camera-at-their-waist.jpg"
    }

    static var urlStringMock9: String {
        // swiftlint:disable:next line_length
        "person-silhouetted-on-a-horse-in-a-open-field.jpg?width=925&format=pjpg&exif=0&iptc=0%201x,%20https://burst.shopifycdn.com/photos/person-silhouetted-on-a-horse-in-a-open-field.jpg"
    }
	
	static var urlStringMock10: String {
		// swiftlint:disable:next line_length
		"a-yawing-siamese-cat.jpg?width=925&format=pjpg&exif=0&iptc=0%201x,%20https://burst.shopifycdn.com/photos/a-yawing-siamese-cat.jpg"
	}
	
	static var urlStringMock11: String {
		// swiftlint:disable:next line_length
		"pasta-cooking-kitchen-prep.jpg?width=925&format=pjpg&exif=0&iptc=0%201x,%20https://burst.shopifycdn.com/photos/pasta-cooking-kitchen-prep.jpg"
	}
	
	static var urlStringMock12: String {
		// swiftlint:disable:next line_length
		"resting-on-basketball-court.jpg?width=925&format=pjpg&exif=0&iptc=0%201x,%20https://burst.shopifycdn.com/photos/resting-on-basketball-court.jpg"
	}
	
	static var urlStringMock13: String {
		// swiftlint:disable:next line_length
		"yellow-tools-on-white-and-yellow-background.jpg?width=925&format=pjpg&exif=0&iptc=0%201x,%20https://burst.shopifycdn.com/photos/yellow-tools-on-white-and-yellow-background.jpg"
	}
	
	static var urlStringMock14: String {
		// swiftlint:disable:next line_length
		"bed-with-a-novel-and-mug-set-on-top-of-the-duvet.jpg?width=925&format=pjpg&exif=0&iptc=0%201x,%20https://burst.shopifycdn.com/photos/bed-with-a-novel-and-mug-set-on-top-of-the-duvet.jpg"
	}
	
	static var urlStringMock15: String {
		// swiftlint:disable:next line_length
		"dead-end-road-sign-among-green-tree-branches.jpg?width=925&format=pjpg&exif=0&iptc=0%201x,%20https://burst.shopifycdn.com/photos/dead-end-road-sign-among-green-tree-branches.jpg"
	}
	
	static var urlStringMock16: String {
		// swiftlint:disable:next line_length
		"plugged-in-vintage-tv-on-purple-infinity-background.jpg?width=925&format=pjpg&exif=0&iptc=0%201x,%20https://burst.shopifycdn.com/photos/plugged-in-vintage-tv-on-purple-infinity-background.jpg"
	}
	
	static var urlStringMock17: String {
		// swiftlint:disable:next line_length
		"a-staircase-climbs-a-building.jpg?width=925&format=pjpg&exif=0&iptc=0%201x,%20https://burst.shopifycdn.com/photos/a-staircase-climbs-a-building.jpg"
	}
	
	static var urlStringMock18: String {
		// swiftlint:disable:next line_length
		"landscape-viewing-binoculars.jpg?width=925&format=pjpg&exif=0&iptc=0%201x,%20https://burst.shopifycdn.com/photos/landscape-viewing-binoculars.jpg"
	}
	
	static var urlStringMock19: String {
		// swiftlint:disable:next line_length
		"winding-tree-towers-over-landscape.jpg?width=925&format=pjpg&exif=0&iptc=0%201x,%20https://burst.shopifycdn.com/photos/winding-tree-towers-over-landscape.jpg"
	}
	
	static var urlStringMock20: String {
		// swiftlint:disable:next line_length
		"table-for-two.jpg?width=925&format=pjpg&exif=0&iptc=0%201x,%20https://burst.shopifycdn.com/photos/table-for-two.jpg"
	}
	
	static var urlStringMock21: String {
		// swiftlint:disable:next line_length
		"unmade-dusty-pink-bedding.jpg?width=925&format=pjpg&exif=0&iptc=0%201x,%20https://burst.shopifycdn.com/photos/unmade-dusty-pink-bedding.jpg"
	}
	
	static var urlStringMock22: String {
		// swiftlint:disable:next line_length
		"pink-brick-wall-texture.jpg?width=925&format=pjpg&exif=0&iptc=0%201x,%20https://burst.shopifycdn.com/photos/pink-brick-wall-texture.jpg"
	}
	
	static var urlStringMock23: String {
		// swiftlint:disable:next line_length
		"a-man-and-a-woman-are-framed-against-a-sunny-window.jpg?width=925&format=pjpg&exif=0&iptc=0%201x,%20https://burst.shopifycdn.com/photos/a-man-and-a-woman-are-framed-against-a-sunny-window.jpg"
	}
}
