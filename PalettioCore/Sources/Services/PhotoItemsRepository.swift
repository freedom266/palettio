//
//  PhotoItemsRepository.swift
//  PalettioCore
//
//  Created by Martin Svoboda on 01.12.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import Combine
import _PhotosUI_SwiftUI

public protocol PhotoItemsRepositoring {
	var items: CurrentValueSubject<[PhotoItem], Never> { get }
	
	func insert(newPhoto: PhotosPickerItem?) async
	func delete(item: PhotoItem)
}
	
public final class PhotoItemsRepository: ObservableObject, PhotoItemsRepositoring {

	public var items: CurrentValueSubject<[PhotoItem], Never> = .init([])
	
	// MARK: Inits

	public init() {
		// Load photo items
		let urls = FileManager.default.getContentsOfDirectory()
		for url in urls {
			let item = PhotoItem(url: url)
			items.value.append(item)
		}
	}
	
	// MARK: Public API

	public func insert(newPhoto: PhotosPickerItem?) async {
		guard
			let image = try? await newPhoto?.loadTransferable(type: Data.self),
			let thumbnail = UIImage(data: image)?.thumbnail,
			let url = FileManager.default.addPhotoItem(image: image, thumbnail: thumbnail)
		else { return }
		
		let newItem = PhotoItem(url: url)
		items.value.append(newItem)
	}
	
	public func delete(item: PhotoItem) {
		FileManager.default.removeItemFromDocumentDirectory(url: item.url)
		items.value.delete(item)
	}
}
