//
//  PreviewSizeProvider.swift
//  PalettioCore
//
//  Created by Martin Svoboda on 29.10.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import Combine

public protocol PreviewSizeProvidering {
	var previewSize: CurrentValueSubject<CGSize, Never> { get }
	var trashLocation: CGSize { get }
	
	func setPreviewSize(size: CGSize)
}
	
public final class PreviewSizeProvider: ObservableObject, PreviewSizeProvidering {

	public var previewSize: CurrentValueSubject<CGSize, Never> = .init(.zero)
	
	public var trashLocation: CGSize {
		.trashLocation(size: previewSize.value)
	}
	
	// MARK: Inits
	
	public init() { }
	
	// MARK: Public API
	
	public func setPreviewSize(size: CGSize) {
		previewSize.value = size
	}
}
