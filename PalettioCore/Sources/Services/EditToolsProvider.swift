//
//  EditToolsProvider.swift
//  PalettioCore
//
//  Created by Martin Svoboda on 25.10.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import Combine

public protocol EditToolsProvidering {
	/// Rulers
	var isHorizontalRulerActive: CurrentValueSubject<Bool, Never> { get }
	var isVerticalRulerActive: CurrentValueSubject<Bool, Never> { get }
	var isRotationRulerActive: CurrentValueSubject<Bool, Never> { get }
	
	/// Trash
	var isTrashShowing: CurrentValueSubject<Bool, Never> { get }
	var isTrashOpen: CurrentValueSubject<Bool, Never> { get }
	
	func updateActiveCaption(data: CaptionData)
	func clearActiveCaption()
}

public final class EditToolsProvider: ObservableObject, EditToolsProvidering {
	@Published private var activeCaptionData: CaptionData?
	@Published private var keyboardHeight: CGFloat = 0
	
	public var isHorizontalRulerActive: CurrentValueSubject<Bool, Never> = .init(false)
	public var isVerticalRulerActive: CurrentValueSubject<Bool, Never> = .init(false)
	public var isRotationRulerActive: CurrentValueSubject<Bool, Never> = .init(false)
	
	public var isTrashShowing: CurrentValueSubject<Bool, Never> = .init(false)
	public var isTrashOpen: CurrentValueSubject<Bool, Never> = .init(false)
	
	private var cancellables = Set<AnyCancellable>()
	
	// MARK: Inits
	
	public init() {
		setupBindings()
	}
	
	// MARK: Public API
	
	public func updateActiveCaption(data: CaptionData) {
		activeCaptionData = data
	}
	
	public func clearActiveCaption() {
		isHorizontalRulerActive.value = false
		isVerticalRulerActive.value = false
		
		activeCaptionData = nil
	}
	
	// MARK: Private API
	
	private func showTrash() {
		if !isTrashShowing.value {
			isTrashShowing.value = true
		}
	}
	
	func hideTrash() {
		if isTrashShowing.value {
			isTrashShowing.value = false
		}
		
		if isTrashOpen.value {
			isTrashOpen.value = false
		}
	}
	
	private func checkIfTrashIsOpen() {
		guard let caption = activeCaptionData else { return }
		
		if caption.transform.distanceFromTrash < .distanceForOpenTrash {
			if !isTrashOpen.value {
				isTrashOpen.value = true
			}
		} else {
			if isTrashOpen.value {
				isTrashOpen.value = false
			}
		}
	}
	
	private func checkIfRulersAreActive() {
		guard let caption = activeCaptionData else { return }
		
		if caption.isLockedHorizontaly != isHorizontalRulerActive.value {
			isHorizontalRulerActive.value.toggle()
		}
		
		if caption.isLockedVerticaly != isVerticalRulerActive.value {
			isVerticalRulerActive.value.toggle()
		}
		
		if caption.isLockedByRotation != isRotationRulerActive.value {
			isRotationRulerActive.value.toggle()
		}
	}
	
	private func setupBindings() {
		$activeCaptionData
			.sink { [weak self] data in
				guard let self else { return }
				if data != nil {
					showTrash()
					checkIfTrashIsOpen()
					checkIfRulersAreActive()
				} else {
					hideTrash()
				}
			}
			.store(in: &cancellables)
		
		NotificationCenter.default.keyboardHeightPublisher
			.weakAssign(to: \.keyboardHeight, on: self)
			.store(in: &cancellables)
	}
}
