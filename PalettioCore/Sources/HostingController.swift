//
//  HostingController.swift
//  PalettioCore
//
//  Created by Martin Svoboda on 06.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

// For iOS 15 the `setNavigationBarHidden` method called in `viewDidLoad`
// doesn't work. Hiding the navigation bar from SwiftUI helps.
// https://stackoverflow.com/a/64782757
public final class HostingController<Content: View>: UIHostingController<AnyView> {
    public init(rootView: Content) {
        super.init(rootView: AnyView(rootView.navigationBarHidden(true)))
    }

    @MainActor required dynamic init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
