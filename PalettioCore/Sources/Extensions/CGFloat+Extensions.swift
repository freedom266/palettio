//
//  CGFloat+Extensions.swift
//  Develop
//
//  Created by Martin Svoboda on 27.10.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import Foundation

public extension CGFloat {
	static let locVelocityThreshold = 2.0
	static let rotVelocityThreshold = 1.0
	static let thresholdForSnapToLocRuler = 8.0
	static let thresholdForSnapToRotRuler = 5.0
	static let distanceForOpenTrash = 50.0
	static let maxCaptionScale = 5.0
}

public extension CGFloat {
	var isPositive: Bool { self > 0 }
	
	/// Need value in this format because of animations for rotation (when value is going to zero)
	var inRangePlusMinus180: CGFloat {
		let modularized = fmod(self, 360)
		return if modularized > 180 {
			modularized - 360
		} else if modularized < -180 {
			modularized + 360
		} else {
			modularized
		}
	}
	
	/// Rounds value to the nearest right angle
	var roundToNearestRightAngle: CGFloat {
		let remainder = fmod(self, 90)
		let floorValue = self - remainder
		let ceilValue = floorValue + 90
		return remainder < 45 ? floorValue : ceilValue
	}
}
