//
//  FileManager+Extensions.swift
//  PalettioCore
//
//  Created by Martin Svoboda on 14.10.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

public extension FileManager {
	/// The URL of the document directory.
	var documentDirectory: URL? {
		self.urls(for: .documentDirectory, in: .userDomainMask).first
	}

	func addPhotoItem(image: Data, thumbnail: Data) -> URL? {
		guard let itemDirectory = createDirectoryForNewItem() else { return nil }
		saveData(data: image, dataUrl: itemDirectory.appendingPathComponent(PhotoItem.imagePath))
		saveData(data: thumbnail, dataUrl: itemDirectory.appendingPathComponent(PhotoItem.thumbnailPath))
		return itemDirectory
	}
	
	func saveData(data: Data?, dataUrl: URL) {
		do {
			guard let data else { return }
			try data.write(to: dataUrl)
		} catch let error {
			print("❌ Unable to save data: \(error.localizedDescription)")
		}
	}
	
	/// Remove file or directory. If the path specifies a directory, the content of that directory is recursively removed.
	func removeItemFromDocumentDirectory(url: URL) {
		if self.fileExists(atPath: url.path) {
			do {
				try removeItem(at: url)
			} catch let error {
				print("❌ Unable to remove item: \(error.localizedDescription)")
			}
		} else {
			print("⁉️ Item can't be removed, because url doesn't exists: \(url.path)")
		}
	}
	
	func getContentsOfDirectory() -> [URL] {
		guard let documentDirectory = documentDirectory else { return [] }
		do {
			return try contentsOfDirectory(at: documentDirectory, includingPropertiesForKeys: nil)
		} catch let error {
			print("❌ Unable to get directory contents: \(error.localizedDescription)")
			return []
		}
	}
	
	// MARK: Private helpers
	
	private func createDirectoryForNewItem() -> URL? {
		guard let documentDirectory = documentDirectory else { return nil }
		let itemUrl = documentDirectory.appendingPathComponent(Date.timeStamp, isDirectory: true)
		do {
			try createDirectory(at: itemUrl, withIntermediateDirectories: true)
			return itemUrl
		} catch {
			print("❌ Error creating directory: \(error)")
			return nil
		}
	}
}
