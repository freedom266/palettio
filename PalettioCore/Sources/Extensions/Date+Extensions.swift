//
//  Date+Extensions.swift
//  PalettioCore
//
//  Created by Martin Svoboda on 14.10.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import Foundation

public extension Date {
	static var timeStamp: String {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "yy-MM-dd-HH-mm-ss"
		return dateFormatter.string(from: Date())
	}
}
