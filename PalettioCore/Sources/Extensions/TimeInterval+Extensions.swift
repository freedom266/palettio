//
//  TimeInterval+Extensions.swift
//  PalettioCore
//
//  Created by Martin Svoboda on 28.10.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import Foundation

public extension TimeInterval {
	static let trashAnimDuration = 0.1
	static let deleteAnimDuration = 0.1
	static let editAnimDuration = 0.2
	static let screenshotDelay = 0.2
}
