//
//  UIImage+Extensions.swift
//  PalettioCore
//
//  Created by Martin Svoboda on 14.10.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import UIKit

public extension UIImage {
	var thumbnail: Data? {
		aspectFittedToWidth(200).pngData()
	}
	
	var croppedToSquare: UIImage {
		let edgeLength = min(size.width, size.height)
		let xOffset = (size.width - edgeLength) / 2.0
		let yOffset = (size.height - edgeLength) / 2.0

		let cropRect = CGRect(
			x: xOffset,
			y: yOffset,
			width: edgeLength,
			height: edgeLength
		)

		// Center crop the image
		let croppedCGImage = cgImage?.cropping(to: cropRect)
		
		if let croppedCGImage {
			return UIImage(cgImage: croppedCGImage)
		} else {
			return self
		}
	}
	
	func mergeWith(topImage: UIImage, opacity: CGFloat = 1, mergeArea: CGRect? = nil) -> UIImage {
		let bottomImage = self
		UIGraphicsBeginImageContext(size)
		
		let areaSize = CGRect(x: 0, y: 0, width: bottomImage.size.width, height: bottomImage.size.height)
		bottomImage.draw(in: areaSize)
		
		if let mergeArea {
			topImage.draw(in: mergeArea, blendMode: .normal, alpha: opacity)
		} else {
			topImage.draw(in: areaSize, blendMode: .normal, alpha: opacity)
		}
		
		// swiftlint:disable:next force_unwrapping
		let mergedImage = UIGraphicsGetImageFromCurrentImageContext()!
		UIGraphicsEndImageContext()
		return mergedImage
	}
	
	func rotate(degrees: CGFloat) -> UIImage {
		guard degrees != 0 else { return self }
		let radians = degrees * CGFloat.pi / 180
		let rotatedRect = CGRect(origin: .zero, size: size).applying(CGAffineTransform(rotationAngle: radians))
		let rotatedSize = CGSize(width: rotatedRect.width, height: rotatedRect.height)
		
		UIGraphicsBeginImageContext(rotatedSize)
		if let context = UIGraphicsGetCurrentContext() {
			context.translateBy(
				x: rotatedSize.width / 2,
				y: rotatedSize.height / 2
			)
			context.rotate(by: radians)
			draw(in: CGRect(
				x: -size.width / 2,
				y: -size.height / 2,
				width: size.width,
				height: size.height)
			)
			
			let rotatedImage = UIGraphicsGetImageFromCurrentImageContext()
			UIGraphicsEndImageContext()
			return rotatedImage ?? self
		}
		return self
	}
}

private extension UIImage {
	func aspectFittedToWidth(_ newWidth: CGFloat) -> UIImage {
		guard newWidth < size.width else { return self }
		
		let scale = newWidth / size.width
		let newHeight = size.height * scale
		let newSize = CGSize(width: newWidth, height: newHeight)
		let renderer = UIGraphicsImageRenderer(size: newSize)

		return renderer.image { _ in
			draw(in: CGRect(origin: .zero, size: newSize))
		}
	}
}
