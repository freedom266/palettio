//
//  Notification+Extensions.swift
//  PalettioCore
//
//  Created by Martin Svoboda on 03.11.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import Foundation
import UIKit

extension Notification {
	var keyboardHeight: CGFloat {
		(userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height ?? 0
	}
}
