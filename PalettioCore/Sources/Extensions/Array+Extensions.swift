//
//  Array+Extensions.swift
//  PalettioCore
//
//  Created by Martin Svoboda on 20.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import Foundation

public extension Array where Element: Identifiable {
	mutating func delete(_ element: Element) {
		guard let index = firstIndex(where: { $0.id == element.id }) else { return }
		remove(at: index)
	}
}

extension Array: RawRepresentable where Element: Codable {
    public init?(rawValue: String) {
        guard let data = rawValue.data(using: .utf8),
              let result = try? JSONDecoder().decode([Element].self, from: data)
        else {
            return nil
        }
        self = result
    }

    public var rawValue: String {
        guard let data = try? JSONEncoder().encode(self),
              let result = String(data: data, encoding: .utf8)
        else {
            return "[]"
        }
        return result
    }
}
