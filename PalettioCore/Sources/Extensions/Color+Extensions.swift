//
//  Color+Extensions.swift
//  PalettioUI
//
//  Created by Martin Svoboda on 20.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

extension Color: RawRepresentable, Codable {
    public init?(rawValue: String) {
        guard let data = Data(base64Encoded: rawValue) else {
            self = .black
            return
        }
        do {
            let color = try NSKeyedUnarchiver.unarchivedObject(ofClass: UIColor.self, from: data)
            self = Color(color ?? UIColor.clear)
        } catch {
            self = .black
        }
    }

    public var rawValue: String {
        do {
            let data = try NSKeyedArchiver.archivedData(
				withRootObject: UIColor(self),
				requiringSecureCoding: false
			)
            return data.base64EncodedString()
        } catch {
            return ""
        }
    }
}
