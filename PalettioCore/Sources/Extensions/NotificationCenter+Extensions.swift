//
//  NotificationCenter+Extensions.swift
//  PalettioCore
//
//  Created by Martin Svoboda on 03.11.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import Foundation
import Combine
import UIKit

extension NotificationCenter {
	var keyboardHeightPublisher: AnyPublisher<CGFloat, Never> {
		let willShow = publisher(for: UIApplication.keyboardWillShowNotification).map { $0.keyboardHeight }
		let willHide = publisher(for: UIApplication.keyboardWillHideNotification).map { _ in CGFloat(0) }
		return Publishers.Merge(willShow, willHide).eraseToAnyPublisher()
	}
}
