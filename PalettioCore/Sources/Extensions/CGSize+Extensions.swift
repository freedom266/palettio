//
//  CGSize+Extensions.swift
//  Develop
//
//  Created by Martin Svoboda on 12.10.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import Foundation

extension CGSize {
	func distance(to point: CGSize) -> CGFloat {
		let x = width - point.width
		let y = height - point.height
		return sqrt(x * x + y * y)
	}
}

extension CGSize {
	static func trashLocation(size: CGSize) -> CGSize {
		.init(width: 0, height: size.height / 2 - 50)
	}
}

public extension CGSize {
	static func - (lhs: CGSize, rhs: CGSize) -> CGSize {
		.init(width: lhs.width - rhs.width, height: lhs.height - rhs.height)
	}
}

public extension CGSize {
	var angle: CGFloat {
		let angleRadians = atan2(width, -height)
		let angleDegrees = angleRadians * 180 / .pi
		
		return angleDegrees >= 0 ? angleDegrees : angleDegrees + 360
	}
}
