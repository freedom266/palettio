import ProjectDescription

public let develop = Feature(
    name: "Develop",
    dependencies: [
        .target(name: core.name),
        .target(name: designSystem.name),
        .target(name: resources.name)
    ],
    hasTests: true,
	hasTesting: true
)
