import ProjectDescription

public let gallery = Feature(
    name: "Gallery",
    dependencies: [
        .target(name: core.name),
        .target(name: designSystem.name),
        .target(name: resources.name)
    ],
    hasTests: true,
	hasTesting: true
)
