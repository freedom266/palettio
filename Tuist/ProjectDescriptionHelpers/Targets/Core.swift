import ProjectDescription

private let targetName = "PalettioCore"
private let bundleID = "\(AppSetup.current.moduleBundleIDPrefix).core"

public let core = Target(
    name: targetName,
    platform: .iOS,
    product: .framework,
    bundleId: bundleID,
    infoPlist: .default,
    sources: SourceFilesList(
        globs: [
            .glob(.relativeToRoot("\(targetName)/Sources/**"))
        ]
    ),
    resources: [
        "\(targetName)/Environment/\(Environment.current)/**"
    ],
    entitlements: nil,
    scripts: [
        .swiftlint
    ],
    dependencies: [
        .external(name: "ACKategories"),
        .target(name: resources.name),
    ]
)
