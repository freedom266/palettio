import ProjectDescription

private let targetName = "Palettio"
private let bundleID = AppSetup.current.bundleID
private let appName = [targetName, AppSetup.current.appNameValue].joined(separator: " ")

private var codeSigning: CodeSigning {
    switch Configuration.current {
    case .debug:
        return .init(
            developmentTeam: AppSetup.current.teamID,
            identity: "Apple Development",
            provisioningSpecifier: "" // Xcode will select `Automatic`
        )
    case .beta, .release:
        return .init(
            developmentTeam: AppSetup.current.teamID,
            identity: "Apple Distribution",
            provisioningSpecifier: "jarvis AppStore " + bundleID
        )
    }
}

public let app = Target(
    name: targetName,
    platform: .iOS,
    product: .app,
    bundleId: bundleID,
	deploymentTarget: .iOS(targetVersion: "16.0", devices: [.iphone, .ipad]),
    infoPlist: .extendingDefault(
        with: [
            "CFBundleDisplayName": .string(appName),
            "ITSAppUsesNonExemptEncryption": false,
            "UILaunchStoryboardName": "Launch-Screen",
            "UISupportedInterfaceOrientations": [
				"UIInterfaceOrientationPortrait"
			],
			"UISupportedInterfaceOrientations~ipad": [
				"UIInterfaceOrientationPortrait",
				"UIInterfaceOrientationPortraitUpsideDown"
			],
            "UIUserInterfaceStyle": "Light",
            "CFBundleShortVersionString": "$(MARKETING_VERSION)",
			"NSPhotoLibraryAddUsageDescription": "This app requires access to the photo library.",
			"NSPhotoLibraryUsageDescription": "We need access to photo library so that photos can be selected."
        ]
    ),
    sources: "\(targetName)/Sources/**",
    resources: .init(
        resources: [
            "\(targetName)/Resources/**"
        ]
    ),
    scripts: [
        .setBuildNumber
    ],
    dependencies: [
        .target(core),
        .target(designSystem),
		.target(develop),
		.target(gallery)
    ],
    settings: .settings(
        base: [
            "ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES": true,
            "APPLICATION_EXTENSION_API_ONLY": false
        ].merging(codeSigning.settingsDictionary) { $1 },
        configurations: AppSetup.current.projectConfigurations
    )
)

// MARK: - Tests

public let appTests = Target(
    name: targetName + "_Tests",
    platform: .iOS,
    product: .unitTests,
    bundleId: bundleID + ".tests",
    infoPlist: .tests,
    sources: "\(targetName)/Tests/**",
    dependencies: [
        .xctest,
        .target(name: targetName),
        .target(name: testing.name),
    ]
)
