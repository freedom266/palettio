import ProjectDescription

private let targetName = "PalettioResources"
private let bundleID = "\(AppSetup.current.moduleBundleIDPrefix).resources"

public let resources = Target(
    name: targetName,
    platform: .iOS,
    product: .framework,
    bundleId: bundleID,
    infoPlist: .default,
    sources: "\(targetName)/Sources/**",
    resources: "\(targetName)/Resources/**"
)
