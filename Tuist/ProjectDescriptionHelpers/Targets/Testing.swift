import ProjectDescription

// MARK: - Settings
private let targetName = "PalettioTesting"
private let bundleID = "\(AppSetup.current.moduleBundleIDPrefix).testing"

// MARK: - Target
public let testing = Target(
    name: targetName,
    platform: .iOS,
    product: .framework,
    bundleId: "\(bundleID)",
    infoPlist: .default,
    sources: "\(targetName)/Tests/**",
    dependencies: [
        .xctest,
		.external(name: "SnapshotTesting"),
    ]
)
