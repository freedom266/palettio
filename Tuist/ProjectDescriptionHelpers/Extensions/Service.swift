import ProjectDescription

public struct Service {
    public let implementation: Target
    public let interface: Target
    public let tests: Target

    public var allTargets: [Target] { [implementation, interface, tests].compactMap { $0 } }

    public init(
        name: String,
        interfaceDependencies: [TargetDependency],
        dependencies: [TargetDependency],
        hasResources: Bool = false
    ) {
        let mainBundleID = "cz.freedom.palettio." + name.toBundleID()

        interface = .init(
            name: name + "_Interface",
            platform: .iOS,
            product: .framework,
            bundleId: mainBundleID + ".interface",
            sources: "Services/\(name)/Interface/**",
            dependencies: interfaceDependencies
        )

        implementation = .init(
            name: name,
            platform: .iOS,
            product: .framework,
            bundleId: mainBundleID + ".service",
            sources: "Services/\(name)/Sources/**",
            resources: hasResources ? "Services/\(name)/Resources/**" : nil,
            dependencies: dependencies + [
                .target(interface)
            ]
        )

        tests = .init(
            name: implementation.name + "_Tests",
            platform: .iOS,
            product: .unitTests,
            bundleId: mainBundleID + ".unittests",
            sources: "Services/\(name)/Tests/**",
            dependencies: dependencies + [
                .target(implementation)
            ]
        )
    }
}

