//
//  ColorItem.swift
//  PalettioResources
//
//  Created by Martin Svoboda on 11.10.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

public struct ColorItem: Codable {
	private let hex: String
	private let alpha: Double
	
	init(hex: String, alpha: Double = 1) {
		self.hex = hex
		self.alpha = alpha
	}
	
	public var uiColor: UIColor {
		UIColor(hexString: hex).withAlphaComponent(alpha)
	}
	
	public var color: Color {
		Color(hexString: hex).opacity(alpha)
	}
	
	public func opacity(_ opacity: Double) -> ColorItem {
		.init(hex: hex, alpha: opacity)
	}
}

extension ColorItem: ExpressibleByStringLiteral {
	public init(stringLiteral value: StaticString) {
		self.hex = "\(value)"
		self.alpha = 1
	}
}

extension ColorItem: Equatable {
	public static func == (lhs: ColorItem, rhs: ColorItem) -> Bool {
		lhs.hex == rhs.hex && lhs.alpha == rhs.alpha
	}
}

public extension ColorItem {
	
	// MARK: System colors
	
	/// #111111
	static var accentPrimary: ColorItem = "111111"
	
	/// #F4F4F8
	static var backgroundsPrimary: ColorItem = "F4F4F8"
	/// #FFFFFF
	static var backgroundsSecondary: ColorItem = "FFFFFF"
	/// #9E9E9E
	static var backgroundsTertiary: ColorItem = "CECECE"
	
	/// #000000
	static var foregroundsPrimary: ColorItem = "000000"
	/// #7D7986
	static var foregroundsSecondary: ColorItem = "7D7986"

	/// #34C759
	static var successPrimary: ColorItem = "34C759"
	/// #FF3B30
	static var failPrimary: ColorItem = "FF3B30"
	
	/// #FFFFFF
	static var deletePrimary: ColorItem = "FFFFFF"

	/// #666666, alpha: 0.35
	static var shadowPrimary: ColorItem = .init(hex: "666666", alpha: 0.35)
	
	// MARK: Basic colors
	
	/// #FFFFFF
	static var basicWhite: ColorItem = "FFFFFF"
	/// #000000
	static var basicBlack: ColorItem = "000000"
	
	// MARK: Palette colors
	
	/// #0BD776
	static var pastelGreen: ColorItem = "0BD776"
	/// #FF96F7
	static var pastelPink: ColorItem = "FF96F7"
	/// #4996F7
	static var pastelBlue: ColorItem = "4996F7"
	/// #FFDD11
	static var pastelYellow: ColorItem = "FFDD11"
	/// #FF6663
	static var pastelRed: ColorItem = "FF6663"
	
	/// #FF2800
	static var ferrariRed: ColorItem = "FF2800"
	
	/// #333333
	static var spaceBlack: ColorItem = "333333"
}

public extension View {
	func foregroundStyle(_ primaryItem: ColorItem, _ secondaryItem: ColorItem) -> some View {
		foregroundStyle(primaryItem.color, secondaryItem.color)
	}
	
	func foregroundColor(_ colorItem: () -> ColorItem) -> some View {
		foregroundStyle(colorItem().color)
	}

	func foregroundColor(_ colorItem: ColorItem) -> some View {
		foregroundStyle(colorItem.color)
	}

	func backgroundColor(_ colorItem: ColorItem) -> some View {
		background(colorItem.color)
	}
	
	func tint(_ colorItem: ColorItem) -> some View {
		tint(colorItem.color)
	}
}

public extension Shape {
	func fill(_ color: ColorItem, style: FillStyle = FillStyle()) -> some View {
		fill(color.color, style: style)
	}

	func stroke(_ color: ColorItem, style: StrokeStyle) -> some View {
		stroke(color.color, style: style)
	}

	func stroke(_ color: ColorItem, lineWidth: CGFloat = 1) -> some View {
		stroke(color.color, lineWidth: lineWidth)
	}
}

#if DEBUG
struct ColorItems_Previews: PreviewProvider {
	static let gridColumns = Array(repeating: GridItem(.flexible(), spacing: 0), count: 2)

	static var previews: some View {
		ScrollView {
			LazyVGrid(columns: gridColumns, spacing: 10) {
				color(title: "accent Primary", color: .accentPrimary, textColor: .basicWhite)
				color(stroke: false)
				
				Divider().padding(.vertical)
				Divider().padding(.vertical)
				
				color(title: "backgrounds Primary", color: .backgroundsPrimary)
				color(title: "backgrounds Secondary", color: .backgroundsSecondary)
				color(title: "backgrounds Tertiary", color: .backgroundsTertiary)
				color(stroke: false)

				Divider().padding(.vertical)
				Divider().padding(.vertical)
				
				color(title: "foregrounds Primary", color: .foregroundsPrimary, textColor: .basicWhite)
				color(title: "foregrounds Secondary", color: .foregroundsSecondary)
				
				Divider().padding(.vertical)
				Divider().padding(.vertical)
				
				color(title: "success Primary", color: .successPrimary)
				color(title: "fail Primary", color: .failPrimary)
				
				Divider().padding(.vertical)
				Divider().padding(.vertical)
				
				color(title: "delete Primary", color: .deletePrimary)
				color(title: "shadow Primary", color: .shadowPrimary)
				
				Divider().padding(.vertical)
				Divider().padding(.vertical)
				
				color(title: "pastel Pink", color: .pastelPink)
				color(title: "pastel Green", color: .pastelGreen)
				color(title: "pastel Blue", color: .pastelBlue)
				color(title: "pastel Yellow", color: .pastelYellow)
				color(title: "pastel Red", color: .pastelRed)
				color(stroke: false)
				
				Divider().padding(.vertical)
				Divider().padding(.vertical)
				
				color(title: "ferrari Red", color: .ferrariRed)
				color(title: "space Black", color: .spaceBlack, textColor: .basicWhite)
			}
			.padding(.horizontal)
		}
	}

	static func color(
		title: String = "",
		color: ColorItem = "FFFFFF",
		textColor: ColorItem = "000000",
		stroke: Bool = true
	) -> some View {
		RoundedRectangle(cornerRadius: 5)
			.fill(color)
			.overlay(alignment: .topLeading) {
				Text(title)
					.font(.system(size: 11, weight: .bold))
					.foregroundColor(textColor)
					.padding(8)
			}
			.frame(maxWidth: .infinity, maxHeight: .infinity)
			.aspectRatio(2, contentMode: .fit)
			.background {
				if stroke {
					RoundedRectangle(cornerRadius: 5)
						.stroke(.basicBlack, lineWidth: 4)
				}
			}
			.padding(.horizontal, 5)
	}
}
#endif
