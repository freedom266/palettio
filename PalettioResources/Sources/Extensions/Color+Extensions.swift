//
//  Color+Extensions.swift
//  PalettioResources
//
//  Created by Martin Svoboda on 11.10.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//
// https://blog.eidinger.info/from-hex-to-color-and-back-in-swiftui

import SwiftUI

public extension Color {
	init(hexString: String) {
		var hexSanitized = hexString.trimmingCharacters(in: .whitespacesAndNewlines)
		hexSanitized = hexSanitized.replacingOccurrences(of: "#", with: "")

		var rgb: UInt64 = 0

		var r: CGFloat = 0.0
		var g: CGFloat = 0.0
		var b: CGFloat = 0.0
		let a: CGFloat = 1.0

		let length = hexSanitized.count

		guard Scanner(string: hexSanitized).scanHexInt64(&rgb) else {
			self = .clear
			return
		}

		if length == 6 {
			r = CGFloat((rgb & 0xFF0000) >> 16) / 255.0
			g = CGFloat((rgb & 0x00FF00) >> 8) / 255.0
			b = CGFloat(rgb & 0x0000FF) / 255.0
		} else {
			self = .clear
		}

		self.init(red: r, green: g, blue: b, opacity: a)
	}
}
