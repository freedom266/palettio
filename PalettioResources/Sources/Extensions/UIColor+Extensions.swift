//
//  UIColor+Extensions.swift
//  PalettioResources
//
//  Created by Martin Svoboda on 11.10.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import UIKit

public extension UIColor {
	convenience init(hexString: String) {
		var hexSanitized = hexString.trimmingCharacters(in: .whitespacesAndNewlines)
		hexSanitized = hexSanitized.replacingOccurrences(of: "#", with: "")

		var rgb: UInt64 = 0

		var r: CGFloat = 0.0
		var g: CGFloat = 0.0
		var b: CGFloat = 0.0
		let a: CGFloat = 1.0

		let length = hexSanitized.count

		guard Scanner(string: hexSanitized).scanHexInt64(&rgb) else {
			self.init()
			return
		}

		if length == 6 {
			r = CGFloat((rgb & 0xFF0000) >> 16) / 255.0
			g = CGFloat((rgb & 0x00FF00) >> 8) / 255.0
			b = CGFloat(rgb & 0x0000FF) / 255.0
		} else {
			self.init()
		}

		self.init(red: r, green: g, blue: b, alpha: a)
	}
}
