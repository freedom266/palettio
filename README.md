# Palettio

Palettio is a simple photo editor app. It allows users to add a colorful gradient overlay of their choice on any photo they select. In the editing interface, users have access to two color pickers, which they can customize to their liking. Additionally, users can add various captions to their photos and position, rotate, zoom, and resize them. To assist with alignment, Palettio provides a ruler feature, enabling users to align captions both horizontally and vertically. All edits made by the user are automatically saved to the app's memory.

![Palettio Screenshots](AppPreviews/iphone-preview.jpg)

### Features

* Apply a custom colorful gradient overlay on photos.
* Add, position, rotate and resize captions on photos.
* Utilize the ruler feature to align captions horizontally and vertically.
* Automatic saving of all edits to the app's memory.

## Dependency management

App uses [SPM](https://www.swift.org/package-manager/) as dependency manager, integrated using [Tuist Dependencies](https://docs.tuist.io/guides/third-party-dependencies). All of the SPM dependenceis are defined in [Dependencies.swift](https://docs.tuist.io/manifests/dependencies/) file.

For tooling, [Mint](https://github.com/yonaskolb/Mint) dependency manager is used.

### Tools used

* [Tuist](https://github.com/tuist/tuist) - Command line tool which is used to generate, maintain and interact with Xcode project and dependencies
* [ACKategories](https://github.com/AckeeCZ/ACKategories) - A bunch of tools, cocoa subclasses and extensions
* [SwiftLint](https://github.com/realm/SwiftLint) - A tool to enforce Swift style and convention (linter)
* [SnapshotTesting](https://github.com/pointfreeco/swift-snapshot-testing) - Library used to perform snapshot testing of app's UI

## Architecture

The application follows the MVVM-C (Model-View-ViewModel-Coordinator) architecture pattern.

### Core modules

1. **Palettio** - main app module
2. **PalettioCore** - core module, which contains sources shared across the app.
3. **PalettioUI** - contains all the important UI elements extensions, set of pre-defined colors, fonts and UI elements reused across the app
4. **PalettioResources** - contains all the resources (animations, assets, fonts , localizations etc.) shared across the app
5. **PalettioTesting** - contains shared testing sources

### Feature modules

Every feature module is located in its separate folder inside **Features** directory, handles a single feature functionality and contains sources relevant only for the given feature module. 

## Snapshot testing

Snapshot testing is implemented using [swift-snapshot-testing](https://github.com/pointfreeco/swift-snapshot-testing). When an assertion first runs, a snapshot is automatically recorded to disk and the test will fail, printing out the file path of any newly-recorded reference. Repeat test runs will load this reference and compare it with the runtime value. If they don't match, the test will fail and describe the difference. Failures can be inspected from Xcode's Report Navigator or by inspecting the file URLs of the failure. 

Snapshot tests should be run on **iPhone 13 Pro** simulator to preserve consistency. 

## Screenshots

![Palettio Screenshots](AppPreviews/ipad-preview.jpg)

## Requirements

- Xcode 15 or later
- iOS 16.0 or later

## Installation & Build process

Follow these steps to get started with Palettio:

1. First, you need to have Mint and Tuist installed. If you haven't already, you can install it using [these instructions for Mint](https://github.com/yonaskolb/Mint#installing) and [these instructions for Tuist](https://github.com/tuist/tuist#install-%EF%B8%8F).

2. Clone this repository to your local machine.
```bash
git clone git@gitlab.com:freedom266/palettio.git
```
3. Go to project folder.
```bash
cd palettio/
```
4. Fetch dependencies
```bash
tuist fetch
```
5. Generate project.
```bash
tuist generate
```

## Author

Palettio is developed and maintained by Martin Svoboda.

## Contributions

We welcome contributions from the community! If you have any ideas for improvement or find any issues, please feel free to create a fork of this repository and submit your changes via a pull request.

### License

Rick and Wiki is completely free and distributed under the MIT License. For more information, see the [LICENSE](LICENSE) file.

## Support

For any inquiries, bug reports, or support requests, please contact us via email at [support@palettio.com](mailto:martin.svoboda026@gmail.com).
